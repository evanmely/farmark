package com.binax.smartfarmer.model;

public class MarketModel {
    int id;
    String name;
    String sell_status;
    String owner_id;
    String breed;
    String category;
    String amount;
    String milk;
    String pic;

    public MarketModel(int id, String name, String owner_id, String breed, String amount,String pic,String milk) {
        this.id = id;
        this.name = name;
        this.owner_id = owner_id;
        this.breed = breed;
        this.amount = amount;
        this.milk = milk;
        this.pic=pic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSell_status() {
        return sell_status;
    }

    public void setSell_status(String sell_status) {
        this.sell_status = sell_status;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMilk() {
        return milk;
    }

    public void setMilk(String milk) {
        this.milk = milk;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
