package com.binax.smartfarmer.model;
public class FarmProduceModel {
    private  int id;
    private String name;
    private String imageResource;
    private String price;
    private String quantity;

    public FarmProduceModel(int id, String name, String imageResource, String price, String quantity) {
        this.id = id;
        this.name = name;
        this.imageResource = imageResource;
        this.price = price;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageResource() {
        return imageResource;
    }

    public void setImageResource(String imageResource) {
        this.imageResource = imageResource;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}