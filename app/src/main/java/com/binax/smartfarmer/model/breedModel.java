package com.binax.smartfarmer.model;
public class breedModel {
    private int id;
    private String name;
    private String owner_id;
    private String dam;
    private String sire;
    private String milk_volume;
    private String date_of_birth;
    private String price;
    //private String type;
    private String reg_no;
    private String pic;

    public breedModel(int id, String name, String owner_id, String dam, String sire, String milk_volume,String price, String reg_no, String pic) {
        this.id = id;
        this.name = name;
        this.owner_id = owner_id;
        this.dam = dam;
        this.sire = sire;
        this.milk_volume = milk_volume;
        this.price = price;
        //this.type = type;
        this.reg_no = reg_no;
        this.pic = pic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getDam() {
        return dam;
    }

    public void setDam(String dam) {
        this.dam = dam;
    }

    public String getSire() {
        return sire;
    }

    public void setSire(String sire) {
        this.sire = sire;
    }

    public String getMilk_capacity() {
        return milk_volume;
    }

    public void setMilk_volume(String milk_volume) {
        this.milk_volume = milk_volume;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }

    public String getReg_no() {
        return reg_no;
    }

    public void setReg_no(String reg_no) {
        this.reg_no = reg_no;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
