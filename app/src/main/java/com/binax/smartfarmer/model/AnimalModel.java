package com.binax.smartfarmer.model;

import com.binax.smartfarmer.Activities.ServiceprovidersActivity;

public class AnimalModel {
    private int id;
    private String name;
    private String dam;
    private String sire;
    private String milk_capacity;
    private String date_of_birth;
    private String breed;
    private String reg_no;
    private String pic;


    public AnimalModel( int id ,String name, String dam, String sire, String milk_capacity, String date_of_birth, String reg_no, String pic) {
        this.id = id;
        this.name = name;

        this.dam = dam;
        this.sire = sire;
        this.milk_capacity = milk_capacity;
        this.date_of_birth = date_of_birth;
       //this.breed = breed;
        this.reg_no = reg_no;
        this.pic = pic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDam() {
        return dam;
    }

    public void setDam(String dam) {
        this.dam = dam;
    }

    public String getSire() {
        return sire;
    }

    public void setSire(String sire) {
        this.sire = sire;
    }

    public String getMilk_capacity() {
        return milk_capacity;
    }

    public void setMilk_capacity(String milk_capacity) {
        this.milk_capacity = milk_capacity;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getReg_no() {
        return reg_no;
    }

    public void setReg_no(String reg_no) {
        this.reg_no = reg_no;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
