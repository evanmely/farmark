package com.binax.smartfarmer.controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.helpers.session_manager;
import com.binax.smartfarmer.model.AnimalModel;
import com.binax.smartfarmer.model.ServiceProviders;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ServiceProviderAdapter  extends  RecyclerView.Adapter<ServiceProviderAdapter.househelpsViewHolder>{

private static Context ctx;
private List<ServiceProviders>HouseList;
private List<ServiceProviders> HouseListFiltered;
        RequestOptions option;
        session_manager session;
public  int id;
    private static final String TAG = "ServiceProviderAdapter";



static ProgressDialog proDialog;
private OnItemClickListener mlistener;

public interface OnItemClickListener{
    void OnItemClick(int position);
}
    public  void setOnItemClickListener(ServiceProviderAdapter.OnItemClickListener listener){
        mlistener= listener;

    }
    public ServiceProviderAdapter(Context ctx, List<ServiceProviders> HouseList) {
        this.ctx = ctx;
        this.HouseList = HouseList;
        option = new RequestOptions().centerCrop().placeholder(R.drawable.personn).error(R.drawable.personn);
    }
    @NonNull
    @Override
    public ServiceProviderAdapter.househelpsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(R.layout.prof_list, null);
        return new ServiceProviderAdapter.househelpsViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ServiceProviderAdapter.househelpsViewHolder holder, final int i) {
        final ServiceProviders prof = HouseList.get(i);
        String thumbnail = HouseList.get(i).getPicture();
        Glide.with(ctx)
                .applyDefaultRequestOptions(new RequestOptions()
                        .placeholder(R.drawable.personn)
                        .error(R.drawable.personn))
                .load("https://binax.co.ke/smartfarm/v1/api/uploads/"+thumbnail)
                .into(holder.imageView);

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(ctx, Locale.getDefault());
        String knownName="";
        try {
            addresses = geocoder.getFromLocation(prof.getLatitude(), prof.getLongitude(), 1);
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            knownName = addresses.get(0).getFeatureName();
            Log.e(TAG, "knownName: "+city);



        } catch (IOException e) {
            e.printStackTrace();
        }


        holder.fname.setText(prof.getFirst_name() +" "+  prof.getLast_name());
        holder.txtlocation.setText(String.valueOf(""+knownName));
        holder.cat.setText(  "Service Provider");
        // id =(String.valueOf(househelpfilter.getId()));
        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone_no= prof.getPhone().replaceAll("-", "");
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+phone_no));
                ctx.startActivity(intent);
            }
        });
//        holder.mylayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent=new Intent(ctx,househelp_details.class);
//                intent.putExtra("id",househelpfilter.getId());
//                ctx.startActivity(intent);
//            }
//        });
//        holder.imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                Intent intent=new Intent(ctx,.class);
////                intent.putExtra("id",househelpfilter.getId());
////                ctx.startActivity(intent);
//            }
//        });

//        if (i== HouseListFiltered.size() - 1  ){
//            onBottomReachedListener.onBottomReached(i);
//
//        }
    }
    @Override
    public int getItemCount() {
        return HouseList.size();
    }





class househelpsViewHolder extends  RecyclerView.ViewHolder{
    TextView fname, textdob, cat, txtlocation;
    ImageView imageView;
    CardView mycardview;
    RatingBar myrating;
    LinearLayout myLinearLayout;
    Button call;

    public househelpsViewHolder(@NonNull View itemView) {
        super(itemView);
        fname = itemView.findViewById(R.id.new_names);
        // textdob = itemView.findViewById(R.id.new_dob);
        txtlocation = itemView.findViewById(R.id.txtlocation);
        call = itemView.findViewById(R.id.btnrequst);
        // texttype= itemView.findViewById(R.id.new_type);
        mycardview= itemView.findViewById(R.id.mycardview);
        cat= itemView.findViewById(R.id.cat);
        imageView = itemView.findViewById(R.id.new_image);
        myrating = itemView.findViewById(R.id.myrating_bar);
        myLinearLayout = itemView.findViewById(R.id.myLinearLayout);
        //Bitmap bmp = imageView.getDrawingCache();
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mlistener!=null){
                    int position= getAdapterPosition();
                    if(position!=RecyclerView.NO_POSITION){
                        mlistener.OnItemClick(position);
                    }
                }
            }
        });
    }


}


}