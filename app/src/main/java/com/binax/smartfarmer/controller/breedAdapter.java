package com.binax.smartfarmer.controller;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.binax.smartfarmer.Activities.BullProfile;
import com.binax.smartfarmer.Activities.breed;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.model.AnimalModel;
import com.binax.smartfarmer.model.breedModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import android.widget.*;
import java.util.ArrayList;
import java.util.List;

public class breedAdapter extends  RecyclerView.Adapter<breedAdapter.househelpsViewHolder> implements Filterable{

    private Context ctx;
    private static final String TAG = "breedAdapter";
    private List<breedModel>HouseList;
    private List<breedModel> HouseListFiltered;
    RequestOptions option;
    private OnItemClickListener mlistener;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    HouseListFiltered = HouseList;
                } else {
                    List<breedModel> filteredList = new ArrayList<>();
                    for (breedModel row : HouseList) {
                        // here we are looking for placement type , location ,, match
                        if ( row.getName().toLowerCase().contains(charString.toLowerCase())||
                                row.getDam().toLowerCase().contains(charString.toLowerCase()) || row.getSire().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    HouseListFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = HouseListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                HouseListFiltered = (ArrayList<breedModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface OnItemClickListener{
        void OnItemClick(int position);
    }
    public  void setOnItemClickListener(OnItemClickListener listener){
        mlistener= listener;

    }
    public breedAdapter(Context ctx, List<breedModel> HouseList) {
        this.ctx = ctx;
        this.HouseList = HouseList;
        this.HouseListFiltered = HouseList;
        option = new RequestOptions().centerCrop().placeholder(R.drawable.personn).error(R.drawable.personn);
    }
    @NonNull
    @Override
    public househelpsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(R.layout.breed_list, null);
        return new househelpsViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final househelpsViewHolder holder, final int i) {
        final breedModel househelpfilter = HouseListFiltered.get(i);
        String thumbnail = HouseList.get(i).getPic();
        Glide.with(ctx)
                .applyDefaultRequestOptions(new RequestOptions()
                        .placeholder(R.drawable.cow1)
                        .error(R.drawable.cow1))
                .load("https://binax.co.ke/smartfarm/v1/api/uploads/"+thumbnail)
                .into(holder.imageView);

        holder.name.setText(househelpfilter.getName());
        holder.milk.setText(String.valueOf(househelpfilter.getMilk_capacity() +"L"));
        holder.sire.setText(String.valueOf(househelpfilter.getSire()));
        holder.price.setText(String.valueOf(househelpfilter.getPrice()));

holder.mylayout.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent=new Intent(ctx, BullProfile.class);
        Log.e(TAG, "id: "+househelpfilter.getOwner_id());
        intent.putExtra("bull_id" ,househelpfilter.getId());
        intent.putExtra("owner_id" ,househelpfilter.getOwner_id());
        ctx.startActivity(intent);
    }

});
holder.contact_s.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent=new Intent(ctx, BullProfile.class);
        Log.e(TAG, "id: "+househelpfilter.getOwner_id());
        intent.putExtra("bull_id" ,househelpfilter.getId());
        intent.putExtra("owner_id" ,househelpfilter.getOwner_id());
        ctx.startActivity(intent);
    }
});

    }
    @Override
    public int getItemCount() {
        return HouseListFiltered.size();
    }


    class househelpsViewHolder extends  RecyclerView.ViewHolder {
        TextView name, age, milk ,price,sire;
        ImageView imageView;
        LinearLayout mylayout;
        Button contact_s;
        //ImageButton mImageButton;

        public househelpsViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.myname);
            age = itemView.findViewById(R.id.age);
            milk = itemView.findViewById(R.id.mymilk);
            price=itemView.findViewById(R.id.price);
            sire= itemView.findViewById(R.id.mysire);
            contact_s= itemView.findViewById(R.id.contact_s);
            imageView=itemView.findViewById(R.id.imageview);
         mylayout=itemView.findViewById(R.id.mlayout);
            //Bitmap bmp = imageView.getDrawingCache();
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mlistener!=null){
                        int position= getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            mlistener.OnItemClick(position);
                        }
                    }
                }
            });
        }

    }


}

