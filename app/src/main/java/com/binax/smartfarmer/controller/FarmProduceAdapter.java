package com.binax.smartfarmer.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.binax.smartfarmer.R;
import com.binax.smartfarmer.model.FarmProduceModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class FarmProduceAdapter extends RecyclerView.Adapter<FarmProduceAdapter.CustomViewHolder> {
    private Context mContext;
    private List<FarmProduceModel> farm;

    public FarmProduceAdapter(List<FarmProduceModel> farm, Context mContext) {
        this.mContext = mContext;
        this.farm = farm;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.farm_produce_items, parent, false);

        return new CustomViewHolder(itemView);
    }

    /**
     *  Populate the views with appropriate Text and Images
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final FarmProduceAdapter.CustomViewHolder holder, int position) {
        FarmProduceModel produce = farm.get(position);
        String thumbnail= produce.getImageResource();
        holder.name.setText(produce.getName());
        holder.price.setText(produce.getPrice());
        holder.quantity.setText(produce.getQuantity());
        Glide.with(mContext)
                .applyDefaultRequestOptions(new RequestOptions()
                        .placeholder(R.drawable.apple)
                        .error(R.drawable.apple))
                .load("https://binax.co.ke/smartfarm/v1/api/uploads/"+thumbnail)
                .into(holder.image);

        //holder.image.setImageResource(pizza.getImageResource());
        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOptionsMenu(holder.menu);
            }
        });
    }

    /**
     * Display options on click of menu icon (3 dots)
     *
     * @param view
     */
    private void showOptionsMenu(View view) {
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.farm_produce_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new  FarmProduceMenuItemClickListener());
        popup.show();
    }

    @Override
    public int getItemCount() {
        return farm.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        public TextView name, price,quantity;
        public ImageView image, menu;

        /**
         * Constructor to initialize the Views
         *
         * @param itemView
         */
        public CustomViewHolder(View itemView) {
            super(itemView);

            name =  itemView.findViewById(R.id.produceName);
            price =itemView.findViewById(R.id.producePrice);
            price =itemView.findViewById(R.id.producePrice);
            quantity = itemView.findViewById(R.id.quantity);
            menu = itemView.findViewById(R.id.menuDots);
            image=itemView.findViewById(R.id.produceImage);
        }
    }

    private class FarmProduceMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
        /**
         * Display Toast message on click of the options in the menu
         *
         * @param item
         * @return
         */
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.order_now:
                    Toast.makeText(mContext, "Order Now", Toast.LENGTH_SHORT).show();
                    return true;
            }
            return false;
        }
    }
}