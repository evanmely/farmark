package com.binax.smartfarmer.controller;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.binax.smartfarmer.Activities.FarmMachinery;
import com.binax.smartfarmer.Activities.Market;
import com.binax.smartfarmer.Activities.MarketDashboard;
import com.binax.smartfarmer.Activities.MyAnimals;
import com.binax.smartfarmer.Activities.MyFarm;
import com.binax.smartfarmer.Activities.ServiceprovidersActivity;
import com.binax.smartfarmer.Activities.dashboard;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.model.dashboardModel;
import com.bumptech.glide.Glide;

import java.util.List;
public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder> {

    private Context mContext;
    private List<dashboardModel> dashboardList;

    Typeface tf;
    Dialog dl;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvtitle;
        ImageView imgMenu;
        CardView cdview;

        public MyViewHolder(View view) {
            super(view);

            tvtitle =  view.findViewById(R.id.tvmenutitle);

            imgMenu=view.findViewById(R.id.imgmenu);

            cdview = view.findViewById(R.id.card_view);
        }
    }

    public DashboardAdapter(Context mContext, List<dashboardModel> dashboardList) {
        this.mContext = mContext;
        this.dashboardList = dashboardList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_menu, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        //FetchImages();

        //imageserver=imageList;
//        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/saira_regular.ttf");
//        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/proximanovaregular.ttf");
//        holder.tvtitle.setTypeface(tf);

        final dashboardModel mydashboard = dashboardList.get(position);
        holder.tvtitle.setText(mydashboard.getTitle());

        Glide.with(mContext)
                .load(mydashboard.getImg())
                .into(holder.imgMenu);

        holder.cdview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(position){
                    case 0:
                        //Toast.makeText(mContext, "clicked", Toast.LENGTH_SHORT).show();
                        mContext.startActivity(new Intent(mContext, MyFarm.class));
                        break;
                    //Second button click
                    case 1:
                        mContext.startActivity(new Intent(mContext, MarketDashboard.class));
                        break;
                    case 2:
                        Toast.makeText(mContext, "coming soon ", Toast.LENGTH_SHORT).show();
                        //third button click
                        break;
                    case 3:
                        dl = new Dialog(mContext);
                        dl.setContentView(R.layout.alert_dialog1);

                        Button btn1 =  dl.findViewById(R.id.activity_alert1);
                        Button btn2 =  dl.findViewById(R.id.activity_alert2);
                        btn1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                        mContext.startActivity(new Intent(mContext, ServiceprovidersActivity.class));
                            }
                        });
                        btn2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mContext.startActivity(new Intent(mContext, FarmMachinery.class));

                            }
                        });
                        dl.show();

                        //fourth button click
                        break;



                    default:
                        break;
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return dashboardList.size();
    }

}