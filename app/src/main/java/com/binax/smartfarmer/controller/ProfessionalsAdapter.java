package com.binax.smartfarmer.controller;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.Activities.MyAnimals;
import com.binax.smartfarmer.Activities.breed;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.helpers.session_manager;
import com.binax.smartfarmer.model.AnimalModel;
import com.binax.smartfarmer.model.ProfessionalsModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import android.widget.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfessionalsAdapter extends  RecyclerView.Adapter<ProfessionalsAdapter.househelpsViewHolder> implements Filterable{

    private static Context ctx;
    private List<ProfessionalsModel>HouseList;
    private List<ProfessionalsModel> HouseListFiltered;
    RequestOptions option;
    session_manager session;
    public  int id;



    static ProgressDialog proDialog;
    private OnItemClickListener mlistener;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    HouseListFiltered = HouseList;
                } else {
                    List<ProfessionalsModel> filteredList = new ArrayList<>();
                    for (ProfessionalsModel row : HouseList) {
                        // here we are looking for placement type , location ,, match
                        if ( row.getEmail().toLowerCase().contains(charString.toLowerCase())||
                                row.getFirst_name().toLowerCase().contains(charString.toLowerCase()) || row.getLast_name().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    HouseListFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = HouseListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                HouseListFiltered = (ArrayList<ProfessionalsModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface OnItemClickListener{
        void OnItemClick(int position);
    }
    public  void setOnItemClickListener(OnItemClickListener listener){
        mlistener= listener;

    }
    public ProfessionalsAdapter(Context ctx, List<ProfessionalsModel> HouseList) {
        this.ctx = ctx;
        this.HouseList = HouseList;
        this.HouseListFiltered = HouseList;
        option = new RequestOptions().centerCrop().placeholder(R.drawable.personn).error(R.drawable.personn);
    }
    @NonNull
    @Override
    public househelpsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(R.layout.profile_row_item, null);
        return new househelpsViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final househelpsViewHolder holder, final int i) {
        final ProfessionalsModel househelpfilter = HouseListFiltered.get(i);
        //String thumbnail = HouseList.get(i)();
        Glide.with(ctx)
                .applyDefaultRequestOptions(new RequestOptions()
                        .placeholder(R.drawable.user)
                        .error(R.drawable.user))
                .load("https://binax.co.ke/smartfarm/v1/api/uploads/")
                .into(holder.imageView);

        holder.name.setText(househelpfilter.getFirst_name() + househelpfilter.getLast_name());
        holder.email.setText(househelpfilter.getEmail());
        holder.phone.setText(String.valueOf(househelpfilter.getPhone()));
        holder.ward.setText(" "+househelpfilter.getWard_id());
        // id =(String.valueOf(househelpfilter.getId()));
        id=househelpfilter.getId();
//        holder.mImageButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//             //do some code
//            }
//        });
    }
    @Override
    public int getItemCount() {
        return HouseListFiltered.size();
    }


    class househelpsViewHolder extends  RecyclerView.ViewHolder{
        TextView name, email, phone ,ward,sire;
        ImageView imageView;
        LinearLayout mylayout;
        ImageButton mImageButton;

        public househelpsViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.myname);
            email = itemView.findViewById(R.id.myemail);
            phone = itemView.findViewById(R.id.myphone);
            ward=itemView.findViewById(R.id.mylocation);
            sire= itemView.findViewById(R.id.mysire);
            imageView=itemView.findViewById(R.id.imageview);
            //mImageButton= (ImageButton) itemView.findViewById(R.id.imageButton);
            //Bitmap bmp = imageView.getDrawingCache();
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mlistener!=null){
                        int position= getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            mlistener.OnItemClick(position);
                        }
                    }
                }
            });
        }


    }


}

