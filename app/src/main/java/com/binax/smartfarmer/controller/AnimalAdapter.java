package com.binax.smartfarmer.controller;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.Activities.MyAnimals;
import com.binax.smartfarmer.Activities.breed;
import com.binax.smartfarmer.Activities.login;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.helpers.AppController;
import com.binax.smartfarmer.helpers.session_manager;
import com.binax.smartfarmer.model.AnimalModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import android.widget.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AnimalAdapter extends  RecyclerView.Adapter<AnimalAdapter.househelpsViewHolder> implements Filterable{

    private static Context ctx;
    private List<AnimalModel>HouseList;
    private List<AnimalModel> HouseListFiltered;
    RequestOptions option;
    session_manager session;
    private  String name;
    public  int id;
    int MY_SOCKET_TIMEOUT_MS = 200000;
    int MY_DEFAULT_MAX_RETRIES = 0;
    public  String access_token;




    static ProgressDialog proDialog;
    private OnItemClickListener mlistener;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    HouseListFiltered = HouseList;
                } else {
                    List<AnimalModel> filteredList = new ArrayList<>();
                    for (AnimalModel row : HouseList) {
                        // here we are looking for placement type , location ,, match
                        if ( row.getName().toLowerCase().contains(charString.toLowerCase())||
                                row.getDam().toLowerCase().contains(charString.toLowerCase()) || row.getSire().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    HouseListFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = HouseListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                HouseListFiltered = (ArrayList<AnimalModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface OnItemClickListener{
        void OnItemClick(int position);
    }
    public  void setOnItemClickListener(OnItemClickListener listener){
        mlistener= listener;

    }
    public AnimalAdapter(Context ctx, List<AnimalModel> HouseList,String name) {
        this.ctx = ctx;
        this.HouseList = HouseList;
        this.HouseListFiltered = HouseList;
        this.name=name;
        option = new RequestOptions().centerCrop().placeholder(R.drawable.personn).error(R.drawable.personn);
    }
    @NonNull
    @Override
    public househelpsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(R.layout.animal_row, null);
        return new househelpsViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final househelpsViewHolder holder, final int i) {
        final AnimalModel househelpfilter = HouseListFiltered.get(i);
        String thumbnail = HouseList.get(i).getPic();
        Glide.with(ctx)
                .applyDefaultRequestOptions(new RequestOptions()
                        .placeholder(R.drawable.cow)
                        .error(R.drawable.cow))
                .load("https://binax.co.ke/smartfarm/v1/api/uploads/"+thumbnail)
                .into(holder.imageView);

        holder.name.setText(househelpfilter.getName());
        holder.age.setText(househelpfilter.getDate_of_birth());
        holder.milk.setText(String.valueOf(househelpfilter.getMilk_capacity()));
        holder.sire.setText(String.valueOf(househelpfilter.getSire()));
        holder.dam.setText(String.valueOf(househelpfilter.getDam()));
       // id =(String.valueOf(househelpfilter.getId()));
       id=househelpfilter.getId();
        holder.mImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu=new PopupMenu(ctx,holder.mImageButton);
                popupMenu.inflate(R.menu.popup_menu);
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){

                            case R.id.iSell:
                                final EditText txtUrl = new EditText(ctx);
                                // Set the default text to a link of the Queen
                                txtUrl.setHint("50000");
                                new AlertDialog.Builder(ctx)
                                        .setTitle("Price")
                                        .setMessage("Please enter selling price of the animal ")
                                        .setView(txtUrl)
                                        .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                String price = txtUrl.getText().toString();
                                                Map<String, Object> myRequestMap = new HashMap<>();
                                                myRequestMap.put("price", price);
                                                myRequestMap.put("animalId",househelpfilter.getId());
                                                try {
                                                    sell(myRequestMap);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        })
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                            }
                                        })
                                        .show();
//
//                                sell ();
                               // Toast.makeText(ctx, "coming soon", Toast.LENGTH_LONG).show();
                                break;
                            case R.id.insure:
                                Toast.makeText(ctx, "coming soon", Toast.LENGTH_LONG).show();
                                break;
                            case R.id.ibreed:

                                Intent myintent = new Intent(ctx,breed.class);
                                if(!name.equals("cows")){
                                    Toast.makeText(ctx, "We currently don't have breeds for this category,please select cows ", Toast.LENGTH_LONG).show();
                                }else {
                                    myintent.putExtra("sire", househelpfilter.getSire());
                                    ctx.startActivity(myintent);

                                }
                                break;
                        }
                        return false;
                    }
                });
            }
        });





//
    }
    @Override
    public int getItemCount() {
        return HouseListFiltered.size();
    }

    public  void sell (Map<String, Object> requestMap) throws Exception {
        session= new session_manager(ctx);
        proDialog= new ProgressDialog(ctx);
       session= new session_manager(ctx);
       HashMap<String, String> user = session.getUserDetails();
        access_token = user.get(session_manager.KEY_ACCESS_TOKEN);

        proDialog.setTitle("Uploading to market.Please wait...");
        proDialog.setCancelable(false);
        proDialog.show();
        String payload = new Gson().toJson(requestMap);
        JsonObjectRequest request = new JsonObjectRequest( APIs.BaseUrl+"v1/api/market/add",
                new JSONObject(payload),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        proDialog.dismiss();
                        Log.e("", "onResponse: "+response );
                        try {
                            //converting the string to json array object
                            JSONObject obj = new JSONObject(String.valueOf(response));
                            Boolean success =obj.getBoolean("success");
                            if(success.equals(true)){
                                SweetAlertDialog dialog = new SweetAlertDialog(ctx, SweetAlertDialog.SUCCESS_TYPE);
                                dialog.setTitleText("Animal Succesfully sent to market !");
                                dialog.setCancelable(false);
                                dialog.show();

                            }
                            else {
                                SweetAlertDialog dialog = new SweetAlertDialog(ctx, SweetAlertDialog.ERROR_TYPE);
                                dialog.setTitleText("Failed to sent animal to market ,Please try again");
                                dialog.setCancelable(false);
                                dialog.show();
                            }


                        } catch (JSONException e) {
                            Toast.makeText(ctx, " There was an ERROR" + e, Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(" error",error.toString());
                        SweetAlertDialog dialog = new SweetAlertDialog(ctx, SweetAlertDialog.ERROR_TYPE);
                        dialog.setTitleText(" Failed to sent animal to market as it already Exists");
                        dialog.setCancelable(false);
                        dialog.show();
                        //Toast.makeText(ctx, "" , Toast.LENGTH_LONG).show();
                        proDialog.dismiss();
                    }
                })
        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+access_token);
                return headers;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                MY_DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().add(request);
    }

    class househelpsViewHolder extends  RecyclerView.ViewHolder{
        TextView name, age, milk ,dam,sire;
        ImageView imageView;
        LinearLayout mylayout;
        ImageButton mImageButton;

        public househelpsViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.myname);
            age = itemView.findViewById(R.id.age);
            milk = itemView.findViewById(R.id.mymilk);
            dam=itemView.findViewById(R.id.mydam);
            sire= itemView.findViewById(R.id.mysire);
            imageView=itemView.findViewById(R.id.imageview);
            mImageButton= (ImageButton) itemView.findViewById(R.id.imageButton);
            //Bitmap bmp = imageView.getDrawingCache();
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mlistener!=null){
                        int position= getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            mlistener.OnItemClick(position);
                        }
                    }
                }
            });
        }


    }


}

