package com.binax.smartfarmer.controller;


import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.binax.smartfarmer.Activities.breed;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.model.AnimalModel;
import com.binax.smartfarmer.model.MarketModel;
import com.binax.smartfarmer.model.breedModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import android.widget.*;
import java.util.ArrayList;
import java.util.List;

public class MarketAdapter extends  RecyclerView.Adapter<MarketAdapter.animalViewHolder> implements Filterable{

    private Context ctx;
    private List<MarketModel>AnimalList;
    private List<MarketModel> AnimalListFiltered;
    RequestOptions option;
    private OnItemClickListener mlistener;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    AnimalListFiltered = AnimalList;
                } else {
                    List<MarketModel> filteredList = new ArrayList<>();
                    for (MarketModel row : AnimalList) {
                        // here we are looking for placement type , location ,, match
                        if ( row.getName().toLowerCase().contains(charString.toLowerCase())||
                                row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getBreed().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    AnimalListFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = AnimalListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                AnimalListFiltered = (ArrayList<MarketModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface OnItemClickListener{
        void OnItemClick(int position);
    }
    public  void setOnItemClickListener(OnItemClickListener listener){
        mlistener= listener;

    }
    public MarketAdapter(Context ctx, List<MarketModel> AnimalList) {
        this.ctx = ctx;
        this.AnimalList = AnimalList;
        this.AnimalListFiltered = AnimalList;
        option = new RequestOptions().centerCrop().placeholder(R.drawable.personn).error(R.drawable.personn);
    }
    @NonNull
    @Override
    public animalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(R.layout.market_row, null);
        return new animalViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final animalViewHolder holder, final int i) {

        final MarketModel animalfilter = AnimalListFiltered.get(i);

        if(i%2==0){
            holder.mycardview.setBackgroundColor(ContextCompat.getColor(ctx,R.color.white));
        }else{
            holder.mycardview.setBackgroundColor(ContextCompat.getColor(ctx,R.color.dash_top));

        }
        String thumbnail = AnimalList.get(i).getPic();
        Glide.with(ctx)
                .applyDefaultRequestOptions(new RequestOptions()
                        .placeholder(R.drawable.cow1)
                        .error(R.drawable.cow1))
                .load("https://binax.co.ke/smartfarm/v1/api/uploads/"+thumbnail)
                .into(holder.imageView);

        holder.name.setText(animalfilter.getName());
        holder.breed.setText(animalfilter.getBreed());
        holder.amount.setText(animalfilter.getAmount());
        holder.milk.setText(animalfilter.getMilk() +" L");
    }
    @Override
    public int getItemCount() {
        return AnimalListFiltered.size();
    }

    class animalViewHolder extends  RecyclerView.ViewHolder {
        TextView name, amount,milk, breed ;
        ImageView imageView;
        CardView mycardview;
        Button btnrequst;
       // ImageView mImageButton;

        public animalViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            amount = itemView.findViewById(R.id.amount);
            milk = itemView.findViewById(R.id.milk_capacity);
            breed = itemView.findViewById(R.id.breed);
            imageView=itemView.findViewById(R.id.new_image);
            mycardview=itemView.findViewById(R.id.mycardview);
            btnrequst=itemView.findViewById(R.id.btnrequst);
            //mImageButton=  itemView.findViewById(R.id.myImageButton);
            //Bitmap bmp = imageView.getDrawingCache();
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mlistener!=null){
                        int position= getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            mlistener.OnItemClick(position);
                        }
                    }
                }
            });
        }

    }


}

