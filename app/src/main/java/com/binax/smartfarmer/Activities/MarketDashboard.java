package com.binax.smartfarmer.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.binax.smartfarmer.R;

public class MarketDashboard extends AppCompatActivity {
CardView animals_card,farm_produce;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_dashboard);
        animals_card= findViewById(R.id.animals_card);
        farm_produce= findViewById(R.id.farm_produce);
        animals_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Market.class));
            }
        });
        farm_produce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),FarmProduce.class));
            }
        });
    }
}
