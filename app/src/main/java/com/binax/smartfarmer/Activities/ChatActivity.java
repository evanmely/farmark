package com.binax.smartfarmer.Activities;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.binax.smartfarmer.R;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;

public class ChatActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    public static final String ANONYMOUS = "Anonymous";
    public static final int DEFAULT_MSG_LENGTH_LIMIT = 1000;
    public static final int RC_SIGN_IN = 1;
    private static final int RC_PHOTO_PICKER =  2;
    private String loggedInUserName = "";
    private ListView mMessageListView;
    private ListView listView;
    //private MessageAdapter mMessageAdapter;
    private MessageAdapter1 adapter;
    //private ProgressBar mProgressBar;
    //private ImageButton mPhotoPickerButton;
    private EditText mMessageEditText;
    private FloatingActionButton mSendButton;

    private String mUsername;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mMessagesDatabaseReference;
    private ChildEventListener mChildEventListener;
    private FirebaseStorage mFirebaseStorage;
    private StorageReference mChatPhotosStorageReference;

    //Firebase Auth Instance Variables

    /*private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat1);

       // mUsername = getIntent().getStringExtra("Username");

       // Log.d("pkchat","username = " + mUsername);


        mFirebaseDatabase = FirebaseDatabase.getInstance();
        //mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseStorage = FirebaseStorage.getInstance();

        //String groupCode = getIntent().getStringExtra("groupCode");

        //mMessagesDatabaseReference = mFirebaseDatabase.getReference().child(groupCode);
       // mChatPhotosStorageReference = mFirebaseStorage.getReference().child("chat_photos");

        //attachDatabaseReadListener();

        // Initialize references to views
        //mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
       // mMessageListView = (ListView) findViewById(R.id.list);
       // mPhotoPickerButton = (ImageButton) findViewById(R.id.photoPickerButton);
        //mMessageEditText = (EditText) findViewById(R.id.input);
      //  mSendButton = (FloatingActionButton) findViewById(R.id.fab);

        // Initialize message ListView and its adapter
//        final List<ChatMessage> friendlyMessages = new ArrayList<>();
//        //adapter = new MessageAdapter1(this, R.layout.item_message, friendlyMessages);
//        adapter = new MessageAdapter1(ChatActivity.this, ChatMessage.class, R.layout.item_in_message,
//                FirebaseDatabase.getInstance().getReference());
//        mMessageListView.setAdapter(adapter);


        // Initialize progress bar
        //mProgressBar.setVisibility(ProgressBar.INVISIBLE);

        // ImagePickerButton shows an image picker to upload a image for a message
//        mPhotoPickerButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                intent.setType("image/jpeg");
//                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
//                startActivityForResult(Intent.createChooser(intent, "Complete action using"), RC_PHOTO_PICKER);
//            }
//        });

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        final EditText input = (EditText) findViewById(R.id.input);
        listView = (ListView) findViewById(R.id.list);

        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            // Start sign in/sign up activity
            startActivityForResult(AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .build(), RC_SIGN_IN);
        } else {
            // User is already signed in, show list of messages
            showAllOldMessages();
        }

        // Enable Send button when there's text to send
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    fab.setEnabled(true);
                } else {
                    fab.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(DEFAULT_MSG_LENGTH_LIMIT)});

        // Send button sends a message and clears the EditText
//        mSendButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                FriendlyMessage friendlyMessage = new FriendlyMessage(input.getText().toString(), mUsername, null);
//                mMessagesDatabaseReference.push().setValue(friendlyMessage);
//                // Clear input box
//                input.setText("");
//            }
//        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (input.getText().toString().trim().equals("")) {
                    Toast.makeText(ChatActivity.this, "Please enter some texts!", Toast.LENGTH_SHORT).show();
                } else {
                    FirebaseDatabase.getInstance()
                            .getReference()
                            .push()
                            .setValue(new ChatMessage(input.getText().toString(),
                                    FirebaseAuth.getInstance().getCurrentUser().getDisplayName(),
                                    FirebaseAuth.getInstance().getCurrentUser().getUid())
                            );
                    input.setText("");
                }
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Signed in successful!", Toast.LENGTH_LONG).show();
                showAllOldMessages();
            } else {
                Toast.makeText(this, "Sign in failed, please try again later", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_sign_out) {
            AuthUI.getInstance().signOut(this)
                    .addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            Toast.makeText(ChatActivity.this, "You have logged out!", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
        }
        return true;
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode == RC_SIGN_IN){
//            if(resultCode == RESULT_OK){
//                Toast.makeText(this, "Signed In Successfully", Toast.LENGTH_SHORT).show();
//            }
//            else if(resultCode == RESULT_CANCELED){
//                //Toast.makeText(this, "Sign In Cancelled", Toast.LENGTH_SHORT).show();
//                finish();
//            }
//        }
//        else if(requestCode == RC_PHOTO_PICKER && resultCode == RESULT_OK){
//            Uri selectedImageUri = data.getData();
//            StorageReference photoRef = mChatPhotosStorageReference.child(selectedImageUri.getLastPathSegment());
//
//            photoRef.putFile(selectedImageUri).addOnSuccessListener
//                    (this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                        @Override
//                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                            Uri downloadUrl = taskSnapshot.getDownloadUrl();
//                            FriendlyMessage friendlyMessage = new FriendlyMessage(null, mUsername, downloadUrl.toString());
//                            mMessagesDatabaseReference.push().setValue(friendlyMessage);
//                        }
//                    });
//        }
//    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.second_menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch(item.getItemId()){
//            case R.id.sign_out_menu:
//                AuthUI.getInstance().signOut(this);
//                Intent intent = new Intent(ChatActivity.this, SelectActivity.class);
//                startActivity(intent);
//                finish();
//                return true;
//            case R.id.change_group_menu:
//                intent = new Intent(ChatActivity.this, SelectActivity.class);
//                startActivity(intent);
//                finish();
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

//    private void onSignedInInitialize(String username){
//        mUsername = username;
//        attachDatabaseReadListener();
//    }

//    private void onSignedOutCleanup(){
//        mUsername = ANONYMOUS;
//        mMessageAdapter.clear();
//
//    }
//
//    private void attachDatabaseReadListener(){
//        if(mChildEventListener == null){
//            mChildEventListener  = new ChildEventListener() {
//                @Override
//                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                    FriendlyMessage friendlyMessage = dataSnapshot.getValue(FriendlyMessage.class);
//                    mMessageAdapter.add(friendlyMessage);
//                    mMessageListView.setSelection(mMessageListView.getAdapter().getCount()-1);
//                }
//                @Override
//                public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
//                @Override
//                public void onChildRemoved(DataSnapshot dataSnapshot) {}
//                @Override
//                public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
//                @Override
//                public void onCancelled(DatabaseError databaseError) {}
//            };
//            mMessagesDatabaseReference.addChildEventListener(mChildEventListener);
//        }
//    }

    private void detachDatabaseReadListener(){
        if(mChildEventListener!=null) {
            mMessagesDatabaseReference.removeEventListener(mChildEventListener);
            mChildEventListener = null;
        }
    }
    private void showAllOldMessages() {
        loggedInUserName = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Log.d("Main", "user id: " + loggedInUserName);

        adapter = new MessageAdapter1(ChatActivity.this, ChatMessage.class, R.layout.item_in_message,
                FirebaseDatabase.getInstance().getReference());

        listView.setAdapter(adapter);
    }

    public String getLoggedInUserName() {
        return loggedInUserName;
    }
}
