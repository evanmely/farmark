package com.binax.smartfarmer.Activities;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.binax.smartfarmer.R;
import com.binax.smartfarmer.helpers.session_manager;
import com.google.android.material.navigation.NavigationView;

import java.util.HashMap;

public class dashboard extends AppCompatActivity {
    CardView myFarm, myServices, profile,market;
    NavigationView navigation;
    DrawerLayout mdrawerLayout;
    session_manager session;
    ActionBarDrawerToggle drawerToggle;
    private Context mContext;
    private View  mview;
    Dialog dl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        session = new session_manager(this);
        HashMap<String, String> professional = session.getUserDetails();
        //final String professional_name = professional.get(session_manager.KEY_PROFESSIONAL_NAME);
       // Log.e("", "onCreate: " + professional_name);
        profile=findViewById(R.id.myprofile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), user_profile.class));
            }
        });
//
//        myFarm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //build notification
//                NotificationCompat.Builder mBuilder =
//                        new NotificationCompat.Builder(view.getContext())
//                                .setSmallIcon(R.drawable.notification)
//                                .setContentTitle("Simple notification")
//                                .setContentText("This is test of simple notification.");
//// Gets an instance of the NotificationManager service
//                NotificationManager notificationManager =(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
////to post your notification to the notification bar
//                notificationManager.notify(0 , mBuilder.build());
//                startActivity(new Intent(getApplicationContext(), MyFarm.class));
//            }
//        });
//        myServices.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                dl = new Dialog(dashboard.this);
//                dl.setContentView(R.layout.alert_dialog);
//                Button btn1 =  dl.findViewById(R.id.activity_alert1);
//                Button btn2 =  dl.findViewById(R.id.activity_alert2);
//                btn1.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        startActivity(new Intent(getApplicationContext(),ServiceprovidersActivity.class));
//                    }
//                });
//                btn2.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        startActivity(new Intent(getApplicationContext(),FarmMachinery.class));
//
//                    }
//                });
//                dl.show();
//                //ShowDialog(getApplicationContext());
//               // startActivity(new Intent(getApplicationContext(), ServiceprovidersActivity.class));
//            }
//        });
        market= findViewById(R.id.market);
        market.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),MarketDashboard.class));
            }
        });
        initInstances();

//        switch (professional_name) {
//            case "Farmer":
//                break;
//            case "Inseminator":
//
//                break;
//            case "AI provider":
//                break;
//
//        }
//        if (professional_name.equals("Farmer")) {
//            //Toast.makeText(this, "" + professional_name, Toast.LENGTH_SHORT).show();
//        }
    }
//    public void ShowDialog(Context context) {
//
//
//    }
    private void initInstances() {

        mdrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerToggle = new ActionBarDrawerToggle(this, mdrawerLayout, R.string.open, R.string.close);
        mdrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        (getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        navigation = (NavigationView) findViewById(R.id.navigation_view);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.profile:
                        startActivity(new Intent(getApplicationContext(),wallet.class));
                        // add navigation drawer item onclick method here
                        break;
                    case R.id.notifications:
                        addNotification();

                        break;
                    case R.id.services:

                        openDialog();
                        // startActivity(new Intent(getApplicationContext(), Bookings.class));
                        break;
                    case R.id.registerAnimals:
                        startActivity(new Intent(getApplicationContext(), RegisterAnimals.class));
                        break;
                    case R.id.help:
                        // startActivity(new Intent(getApplicationContext(), About.class));
                        break;

                    case R.id.settings:
                        menuItem.setVisible(false);
                        //Do some thing here
                        // add navigation drawer item onclick method here
                        break;
                    case R.id.logout:
                         session.logoutUser();
                        break;
                }
                return true;
            }
        });

    }
    public void openDialog() {
        final Dialog dialog = new Dialog(this); // Context, this, etc.
        dialog.setContentView(R.layout.alert_dialog);;
        dialog.show();
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    // Creates and displays a notification
    //build notification
    public void addNotification() {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notification)
                        .setContentTitle("Evans is interested in your services")
                        .setContentText("please accept.")
                        .setDefaults(Notification.DEFAULT_ALL) // must requires VIBRATE permission
                        .setPriority(NotificationCompat.PRIORITY_HIGH); //must give priority to High, Max which will considered as heads-up notification

    }
}
