package com.binax.smartfarmer.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.binax.smartfarmer.Activities.Register;
import com.binax.smartfarmer.Activities.login;
import com.binax.smartfarmer.R;

public class WelcomeActivity extends AppCompatActivity {
    LinearLayout l1,l2;
    Button btnreg,btnlog;
    Animation uptodown,downtoup;
    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        btnreg = findViewById(R.id.btnregister);
        btnlog = findViewById(R.id.btnlogin);
        l1 =  findViewById(R.id.l1);
        l2 = findViewById(R.id.l2);
        uptodown = AnimationUtils.loadAnimation(this,R.animator.up_to_down);
        downtoup = AnimationUtils.loadAnimation(this,R.animator.down_to_up);
        l1.setAnimation(uptodown);
        l2.setAnimation(downtoup);
        btnlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), login.class));
            }
        });
        btnreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Register.class));
            }
        });
    }
}