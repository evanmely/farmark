package com.binax.smartfarmer.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.controller.AnimalAdapter;
import com.binax.smartfarmer.helpers.session_manager;
import com.binax.smartfarmer.model.AnimalModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class BullProfile extends AppCompatActivity {
    session_manager session;
    String access_token;
    TextView all_names,description,ai_code,milk_capacity,price,calving_ease,dam,sire;
    Button request;
    private static final String TAG = "BullProfile";
    static String phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bull_profile);
        session=new session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
        access_token = user.get(session_manager.KEY_ACCESS_TOKEN);
        all_names=findViewById(R.id.all_names);
        description=findViewById(R.id.description);
        ai_code=findViewById(R.id.ai_code);
        milk_capacity=findViewById(R.id.milk_capacity);
        price=findViewById(R.id.price);
        calving_ease=findViewById(R.id.calving_ease);
        dam=findViewById(R.id.dam);
        sire=findViewById(R.id.sire);
       request=findViewById(R.id.request);
        int bull_id = getIntent().getIntExtra("bull_id",0);
        final String owner_id = getIntent().getStringExtra("owner_id");
        Log.e(TAG, "owner_id: "+owner_id );

        loadBullProfile (bull_id);

        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadUserProfile(owner_id);
            }
        });
    }
    private void loadBullProfile (int bull_id) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading  bull profile ... please wait.");
        pDialog.setCancelable(false);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, APIs.BaseUrl+"v1/api/bull/detail/"+bull_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        Log.e("", "Response:"+response );
                        try {
                            //converting the string to json array object
                            JSONObject obj = new JSONObject(response);
                            JSONObject result= obj.getJSONObject("result");
                            all_names.setText(""+result.getString("name"));
                            description.setText(""+result.getString("description"));
                            ai_code.setText(""+result.getString("ai_code"));
                            milk_capacity.setText(""+result.getString("milk_capacity"));
                            price.setText(""+result.getString("price"));
                            calving_ease.setText(""+result.getString("calving_ease"));
                            dam.setText(""+result.getString("dam"));
                            sire.setText(""+result.getString("sire"));


                        } catch (JSONException e) {
                            //Toast.makeText(getApplicationContext(), " There was an ERROR  " + e, Toast.LENGTH_LONG).show();
                            Log.e(TAG, "onResponse: "+e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(" error",error.toString());
                        //Toast.makeText(getApplicationContext(), " An ERROR occurred" , Toast.LENGTH_LONG).show();
                        pDialog.dismiss();
                    }
                }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id", user_id);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+access_token);
                return headers;
            }

        };
        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
    private void loadUserProfile (String user_id) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading  bull profile ... please wait.");
        pDialog.setCancelable(false);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, APIs.BaseUrl+"v1/api/serviceProvider/"+user_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        Log.e("", "Response:"+response );
                        try {
                            //converting the string to json array object
                            JSONObject obj = new JSONObject(response);
                            JSONObject result= obj.getJSONObject("result");
                            phone = result.getString("phone");

                            String phone_no= phone.replaceAll("-", "");
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:"+phone_no));
                            startActivity(intent);

                        } catch (JSONException e) {
                            //Toast.makeText(getApplicationContext(), " There was an ERROR  " + e, Toast.LENGTH_LONG).show();
                            Log.e(TAG, "onResponse: "+e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(" error",error.toString());
                        //Toast.makeText(getApplicationContext(), " An ERROR occurred" , Toast.LENGTH_LONG).show();
                        pDialog.dismiss();
                    }
                }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id", user_id);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+access_token);
                return headers;
            }

        };
        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
}
