package com.binax.smartfarmer.Activities;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.view.View;
import android.widget.*;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.helpers.session_manager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class user_profile extends AppCompatActivity {
session_manager session;
    String access_token;
    private static final String TAG = "user_profile";
    TextView all_names,phone,email,category,location;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        session=new session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
        access_token = user.get(session_manager.KEY_ACCESS_TOKEN);
        all_names=findViewById(R.id.all_names);
        phone=findViewById(R.id.phone);
        email=findViewById(R.id.email);
        location=findViewById(R.id.location);
        category=findViewById(R.id.specialization_id);
        loadBullProfile ();

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    private void loadBullProfile () {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading  your  profile ... please wait.");
        pDialog.setCancelable(false);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, APIs.BaseUrl+"v1/api/user-details",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        Log.e("", "Response:"+response );
                        try {
                            //converting the string to json array object
                            JSONObject obj = new JSONObject(response);
                            JSONObject result= obj.getJSONObject("result");
                            all_names.setText(""+result.getString("first_name")+" "+result.getString("first_name"));
                            email.setText(""+result.getString("email"));
                            phone.setText(""+result.getString("phone"));
                            Geocoder geocoder;
                            List<Address> addresses;
                            geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            String knownName="";
                            try {
                                addresses = geocoder.getFromLocation(result.getDouble("latitude"), result.getDouble("longitude"), 1);
                                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String country = addresses.get(0).getCountryName();
                                String postalCode = addresses.get(0).getPostalCode();
                                knownName = addresses.get(0).getFeatureName();
//                                Log.e(TAG, "knownName: "+city);



                            } catch (IOException e) {
                                e.printStackTrace();
                            }





                            location.setText(""+knownName);



                        } catch (JSONException e) {
                            //Toast.makeText(getApplicationContext(), " There was an ERROR  " + e, Toast.LENGTH_LONG).show();
                            Log.e(TAG, "onResponse: "+e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(" error",error.toString());
                        //Toast.makeText(getApplicationContext(), " An ERROR occurred" , Toast.LENGTH_LONG).show();
                        pDialog.dismiss();
                    }
                }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                // params.put("id", user_id);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+access_token);
                return headers;
            }

        };
        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
}
