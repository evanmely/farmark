package com.binax.smartfarmer.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.helpers.AppController;
import com.binax.smartfarmer.helpers.session_manager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

//import cn.pedant.SweetAlert.SweetAlertDialog;

public class makePayment extends AppCompatActivity {
session_manager session ;
ProgressDialog proDialog;
Button pay;
    String payableamount= null;
EditText amount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_payment);
       session= new session_manager(this);
        proDialog= new ProgressDialog(this);
        amount= findViewById(R.id.amount);
final String amt= amount.getText().toString();
pay= findViewById(R.id.pay);
pay.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        makepayment(amt);
    }
  });
    }

    // mke payment for the housegirl
    private void makepayment(final String amountPaid) {
        session = new session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
        String tag_string_req = "req_Pay";
        user = session.getUserDetails();
       // final String phone = user.get(session_manager.KEY_PHONE);
       // Log.e("", "getParams: "+phone +amountPaid);
        // progress_dialog();

//        SweetAlertDialog proDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
//        proDialog.getProgressHelper().setBarColor(Color.parseColor("#FFCE511C"));
//        proDialog.setTitleText("Please wait...");
//        proDialog.setCancelable(false);
//        proDialog.show();
        proDialog.setTitle("Making Payment");
        proDialog.show();
        proDialog.setCancelable(false);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                APIs.BaseUrl+"payment/c2b", new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                proDialog.dismiss();

                try {
                    JSONObject jObj = new JSONObject(response);

                   // if (jObj.has("CheckoutRequestID")) {
//                        SweetAlertDialog dialog = new SweetAlertDialog(Payment.this, SweetAlertDialog.SUCCESS_TYPE);
//                        dialog.setTitleText("Success !!! Enter Your Pin and wait for Confirmation  Message");
//                        dialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                            @Override
//                            public void onClick(SweetAlertDialog sweetAlertDialog) {
//                                dialog.dismissWithAnimation();
////                                Intent intent =new Intent(new Intent(Payment.this, Bookings.class));
////                                startActivity(intent);
////                                finish();
//
//
//                            }
                        // });
                        //dialog.show();


                   // }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Payment", "payment Error: " + error.getMessage());
//                SweetAlertDialog pdialog = new SweetAlertDialog(Payment.this, SweetAlertDialog.ERROR_TYPE);
//                pdialog.setTitleText("Error Occurred Please Try again"+hlp_id+"hello"+id);
//                pdialog.show();
                //Toast.makeText(getApplicationContext(), id +"Please wait for transaction to complete "+ hlp_id, Toast.LENGTH_LONG).show();
                proDialog.dismiss();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
               // params.put("phone",phone);
                params.put("amount", amountPaid);
                // params.put("plan_id", "1");
                return params;

            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
