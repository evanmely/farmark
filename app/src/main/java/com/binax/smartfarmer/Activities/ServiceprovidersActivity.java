package com.binax.smartfarmer.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.binax.smartfarmer.R;

public class ServiceprovidersActivity extends AppCompatActivity {
CardView Ai,services;
String type_of_service;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service_providers);
        Ai = findViewById(R.id.AI);
        services = findViewById(R.id.services);

        Ai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type_of_service="AI provider";
                startActivity(new Intent(getApplicationContext(),Professionals.class));
            }
        });
        services.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),ProfList.class));
            }
        });
    }
}
