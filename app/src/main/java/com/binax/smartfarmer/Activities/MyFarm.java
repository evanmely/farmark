package com.binax.smartfarmer.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.helpers.AppController;
import com.binax.smartfarmer.helpers.session_manager;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MyFarm extends AppCompatActivity {
session_manager session;
   // private ProgressDialog pd;
CardView view_animals,register_animals,chat_card;
    String access_token;
    ArrayList<BarDataSet> yAxis;
    ArrayList<BarEntry> yValues;
    ArrayList<String> xAxis1;
    BarEntry values ;
    BarChart chart;
    private static final String TAG = "MyFarm";
    BarData data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_farm);
           // pd = new ProgressDialog(MyFarm.this);
register_animals=findViewById(R.id.reg_animals);
view_animals=findViewById(R.id.view_animals);
chat_card=findViewById(R.id.chat_card);
        chat_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),ChatActivity.class));
            }
        });
view_animals.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(getApplicationContext(),MyAnimals.class));
    }
});
register_animals.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(getApplicationContext(),RegisterAnimals.class));
    }
});


            // Log.d("array",Arrays.toString(fullData));
            chart = (BarChart) findViewById(R.id.chart);
            load_data_from_server();
        session= new session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
        access_token = user.get(session_manager.KEY_ACCESS_TOKEN);
        }

        public void load_data_from_server() {
            //pd.setMessage("loading");
            //pd.show();
            xAxis1 = new ArrayList<>();
            yAxis = null;
            yValues = new ArrayList<>();
            session= new session_manager(this);
            HashMap<String, String> user = session.getUserDetails();
           // final String user_id = user.get(session_manager.KEY_ID);

            StringRequest stringRequest = new StringRequest(Request.Method.GET,
                    APIs.BaseUrl+"v1/api/animals/get/statistics",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("string",response);

                            try {
                           JSONObject jsonObject= new JSONObject(response);
                                JSONArray jsonarray = jsonObject.getJSONArray("result");
                                for(int i=0; i < jsonarray.length(); i++) {

                                    JSONObject jsonobj = jsonarray.getJSONObject(i);

                                    String score = jsonobj.getString("count").trim();
                                    String name = jsonobj.getString("name").trim();

                                    xAxis1.add(name);

                                    values = new BarEntry(Float.valueOf(score),i);
                                    yValues.add(values);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            BarDataSet barDataSet1 = new BarDataSet(yValues, "Animals statistics");
                            barDataSet1.setColor(Color.rgb(0, 82, 159));
                            yAxis = new ArrayList<>();
                            yAxis.add(barDataSet1);
                            String names[]= xAxis1.toArray(new String[xAxis1.size()]);
                            data = new BarData(names,yAxis);
                            chart.setData(data);
                            chart.setDescription("");
                            chart.animateXY(2000, 2000);
                            chart.invalidate();
                            //pd.hide();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(error != null){

                                Toast.makeText(getApplicationContext(), "Something went wrong.", Toast.LENGTH_LONG).show();
                               // pd.hide();
                            }
                        }
                    }

            ){

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer "+access_token);
                    return headers;
                }
            };
            AppController.getInstance().addToRequestQueue(stringRequest);

        }


    }

