package com.binax.smartfarmer.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.controller.AnimalAdapter;
import com.binax.smartfarmer.helpers.session_manager;
import com.binax.smartfarmer.model.AnimalModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyAnimals extends AppCompatActivity{
    JSONArray animals;
    ArrayList<AnimalModel> AnimalList = new ArrayList<>();
    ArrayList<AnimalModel> filteredList ;
    RecyclerView recyclerView;
    AnimalAdapter adapter;
    ProgressDialog proDialog;
    session_manager session;
    Spinner animal_category;
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<String>categoryName;
    String catName=null;
    String id=null;
    String selectedName;
    String category_id=null;
    String access_token;
    private static final String TAG = "MyAnimals";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_animals);
        recyclerView = findViewById(R.id.rec);
        swipeRefreshLayout = findViewById(R.id.main_swiperefresh);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(false);
        proDialog= new ProgressDialog(this);
        animal_category= findViewById(R.id.animal_category);

        session= new session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
        access_token = user.get(session_manager.KEY_ACCESS_TOKEN);


        categoryName=new ArrayList<>();
//        ArrayAdapter<String> category=new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.categories));
//        category.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        animal_category.setAdapter(category);
        loadCategories();
        loadAnimals ();
    }
    private void loadAnimals () {
        session= new session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
        //final String user_id = user.get(session_manager.KEY_ID);
        proDialog.setTitle("Loading Animals . Please wait...");
        proDialog.setCancelable(false);
        proDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, APIs.BaseUrl+"v1/api/animals/get/all",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        proDialog.dismiss();
                        Log.e("", "Response:"+response );
                        try {
                            //converting the string to json array object
                            JSONObject obj = new JSONObject(response);
                            JSONArray animals2= obj.getJSONArray("result");
                            if(animals2.length()>0) {
                            animals = obj.getJSONArray("result");
                                for (int i = 0; i < animals.length(); i++) {
                                    //getting product object from json array
                                    JSONObject animal = animals.getJSONObject(i);
                                    //adding the animal to  view
                                    AnimalList.add(new AnimalModel(
                                            animal.getInt("id"),
                                            animal.getString("name"),
                                            animal.getString("dam"),
                                            animal.getString("sire"),
                                            animal.getString("milk_capacity"),
                                            animal.getString("date_born"),
                                            animal.getString("animal_registration_number"),
                                            animal.getString("picture")
                                    ));
                                }
                                animal_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        selectedName   =  animal_category.getItemAtPosition(animal_category.getSelectedItemPosition()).toString();
                                        // catId=selectedName.split("-")[0]);
                                        String id =selectedName.split("-")[0];
                                        filterAnimals(selectedName);
                                        }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {
                                    }
                                });
                            }

                            else
                            {
                            Toast.makeText(MyAnimals.this, "You do not have animals .Please register them", Toast.LENGTH_SHORT).show();
                            }
                            // creating adapter object and setting it to recyclerview
                            adapter=new AnimalAdapter(MyAnimals.this,AnimalList,"cows");
                            recyclerView.setAdapter(adapter);
                            recyclerView.scheduleLayoutAnimation();
                            adapter.notifyDataSetChanged();
                            proDialog.dismiss();
                            // adapter.setOnItemClickListener(this);
                        } catch (JSONException e) {
                            //Toast.makeText(getApplicationContext(), " There was an ERROR  " + e, Toast.LENGTH_LONG).show();
                            Log.e(TAG, "onResponse: "+e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(" error",error.toString());
                        //Toast.makeText(getApplicationContext(), " An ERROR occurred" , Toast.LENGTH_LONG).show();
                        proDialog.dismiss();
                    }
                }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
               // params.put("id", user_id);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+access_token);
                return headers;
            }

        };
        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }


    public void loadCategories(){
            StringRequest stringRequest=new StringRequest(Request.Method.GET, APIs.BaseUrl+"v1/api/animals/get_category", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try{
                        JSONObject jsonObject=new JSONObject(response);



                        JSONArray jsonArray=jsonObject.getJSONArray("result");
                        for(int i=0;i<jsonArray.length();i++){

                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            catName=jsonObject1.getString("category_name");
                            id=jsonObject1.getString("id");
                            categoryName.add(catName);

                        }


                        animal_category.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, categoryName));

                    }catch (JSONException e){e.printStackTrace();}

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }

            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer "+access_token);
                    return headers;
                }
            };
        Volley.newRequestQueue(this).add(stringRequest);
        }
    private void filterAnimals (final String cat_nam) {
        session= new session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
        //final String user_id = user.get(session_manager.KEY_ID);
        filteredList=new ArrayList<>();
        proDialog.setTitle("Loading Animals . Please wait...");
        proDialog.setCancelable(false);
        proDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, APIs.BaseUrl+"v1/api/animals/get/category/"+cat_nam,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        proDialog.dismiss();
                        Log.e("", "onResponse: "+response );
                        try {
                            //converting the string to json array object
                            JSONObject obj = new JSONObject(response);
                            Boolean available= obj.getBoolean("success");
                            if(available==true) {
                                JSONArray data = obj.getJSONArray("result");
                                for (int i = 0; i < data.length(); i++) {
                                    //getting product object from json array
                                    JSONObject animal = data.getJSONObject(i);
                                    //adding the animal to  view
                                    filteredList.add(new AnimalModel(
                                            animal.getInt("id"),
                                            animal.getString("name"),
                                            animal.getString("dam"),
                                            animal.getString("sire"),
                                            animal.getString("milk_capacity"),
                                            animal.getString("date_born"),
                                            animal.getString("animal_registration_number"),
                                            animal.getString("picture")

                                    ));
                                }
                            }
                            else {
                                Toast.makeText(MyAnimals.this, "It seems you don't have any animals registered,please register them", Toast.LENGTH_SHORT).show();
                            }
                            // creating adapter object and setting it to recyclerview
                            adapter=new AnimalAdapter(MyAnimals.this,filteredList,cat_nam);
                            recyclerView.setAdapter(adapter);
                            recyclerView.scheduleLayoutAnimation();
                            adapter.notifyDataSetChanged();
                            proDialog.dismiss();
                            // adapter.setOnItemClickListener(this);
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), " There was an ERROR   " + e, Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(" error",error.toString());
                        Toast.makeText(getApplicationContext(), " An ERROR occurred" , Toast.LENGTH_LONG).show();
                        proDialog.dismiss();
                    }
                }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+access_token);
                return headers;
            }

        };
        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }


    }
