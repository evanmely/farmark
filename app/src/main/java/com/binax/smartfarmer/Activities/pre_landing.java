package com.binax.smartfarmer.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.helpers.AppController;
import com.binax.smartfarmer.helpers.session_manager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.widget.*;

public class pre_landing extends AppCompatActivity {
    Spinner spinner1,spinner2,spinner3,specialization_spinner;
    Button btnsave;
    JSONArray jsonArray;
    //String Name=null;
    ProgressDialog pDialog;
    RequestQueue requestQueue;
    public String county_id=null;
    public String subcounty_name=null;
    public String ward_name=null;
    session_manager session;
    public String code=null;
    String URL= APIs.BaseUrl+"county/get";
    ArrayList<String> CountyName,specialization;
    ArrayList<String>SubCountyName;
    ArrayList<String>wards;
    private ViewGroup view;
    EditText regNo;
    TextView tvReg;
    public String registration_no;
    String prof_name;
JSONArray ward_array;
            //,SubCountyName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_landing);
        requestQueue= Volley.newRequestQueue(getApplicationContext());
        CountyName=new ArrayList<>();
        SubCountyName=new ArrayList<>();
        wards=new ArrayList<>();
        wards=new ArrayList<>();
        specialization=new ArrayList<>();
        session=new session_manager(this);
        spinner1=findViewById(R.id.county_Name);
        spinner2=findViewById(R.id.sub_county);
        spinner3=findViewById(R.id.ward);
        specialization_spinner=findViewById(R.id.specialization);
       session=new session_manager(this);
       btnsave=findViewById(R.id.save);
       regNo=findViewById(R.id.edit_reg);
       tvReg=findViewById(R.id.tv_reg);
       pDialog =new ProgressDialog(this);

      btnsave.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        savedata();
        //startActivity(new Intent(getApplicationContext(),dashboard.class));
    }
});
        loadCounties(URL);
        loadprofessions();
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
              String county=   spinner1.getItemAtPosition(spinner1.getSelectedItemPosition()).toString();
               //String _id=   spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString();

                if(!county.contains("Select County")){
                    county_id=county.split("-")[0];
                    // Toast.makeText(getApplicationContext(),Name,Toast.LENGTH_LONG).show();
                }
               // Toast.makeText(getApplicationContext(),county,Toast.LENGTH_LONG).show();

                loadSubCounties(county_id);
                                //Toast.makeText(getApplicationContext(),county,Toast.LENGTH_LONG).show();

            }

            @Override

            public void onNothingSelected(AdapterView<?> adapterView) {
            }

        });
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override

            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                subcounty_name=   spinner2.getItemAtPosition(spinner2.getSelectedItemPosition()).toString();
                //String _id=   spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString();
                loadwards(county_id,subcounty_name);
                //Toast.makeText(getApplicationContext(),county,Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }

        });
        //wards
        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                 ward_name =  spinner3.getItemAtPosition(spinner3.getSelectedItemPosition()).toString();
                Toast.makeText(getApplicationContext(),ward_name,Toast.LENGTH_LONG).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        //county_id=county.split("-")[0];
        //
        specialization_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override

            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                prof_name=   specialization_spinner.getItemAtPosition(specialization_spinner.getSelectedItemPosition()).toString();
                //session.createLoginSession("","","","","",prof_name);
                //String _id=   spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString();
                if(!prof_name.equals("Farmer")){
                    regNo.setVisibility(View.VISIBLE);
                    tvReg.setVisibility(View.VISIBLE);
                    // Toast.makeText(getApplicationContext(),Name,Toast.LENGTH_LONG).show();
                }
                else {
                    regNo.setVisibility(View.GONE);
                    tvReg.setVisibility(View.GONE);
                }
                //Toast.makeText(getApplicationContext(),county,Toast.LENGTH_LONG).show();
            }

            @Override

            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }

        });
    }
    private void loadCounties(String url) {
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getBoolean("available")==true){

                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        String county ="Select County";
                        CountyName.add(county);
                        for(int i=0;i<jsonArray.length();i++){

                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            county=jsonObject1.getString("name");
                             code=jsonObject1.getString("code");
//                            Toast.makeText(getApplicationContext(),county_id,Toast.LENGTH_LONG).show();
//
                            Log.e("", "onResponse: "+county );
//                            Log.i("", "onResponse: "+county );

                            Toast.makeText(pre_landing.this, ""+county_id, Toast.LENGTH_SHORT).show();
                            CountyName.add(code+"-"+county);
                           // Log.e("counties", "onResponse: "+county );

                        }
                    }

                    spinner1.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, CountyName));

                }catch (JSONException e){e.printStackTrace();}

            }

        }, new Response.ErrorListener() {

            @Override

            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }

        });

        int socketTimeout = 30000;

        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);

        requestQueue.add(stringRequest);

    }
    private void loadSubCounties(String id) {



        StringRequest stringRequest=new StringRequest(Request.Method.POST , APIs.BaseUrl+"county/get/"+id, new Response.Listener<String>() {

            @Override

            public void onResponse(String response) {
                SubCountyName.clear();
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getBoolean("available")==true){

                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for(int i=0;i<jsonArray.length();i++){

                            JSONObject jsonObject1=jsonArray.getJSONObject(i);

                            String subcounty=jsonObject1.getString("name");
                            Log.e("", "onResponse: "+subcounty );
//                            Log.i("", "onResponse: "+county );
                            Toast.makeText(pre_landing.this, ""+subcounty, Toast.LENGTH_SHORT).show();

                            SubCountyName.add(subcounty);
//                            SubCountyName.add("Select County");

                        }

                    }

                    spinner2.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, SubCountyName));

                }catch (JSONException e){e.printStackTrace();}

            }

        }, new Response.ErrorListener() {

            @Override

            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }

        });

        int socketTimeout = 30000;

        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);

        requestQueue.add(stringRequest);

    }

    private void loadwards(String c_id,String name) {
                StringRequest stringRequest=new StringRequest(Request.Method.POST , APIs.BaseUrl+"county/get/"+c_id+"/"+name, new Response.Listener<String>() {

            @Override

            public void onResponse(String response) {
                wards.clear();
                try{

                    JSONObject jsonObject=new JSONObject(response);

                    if(jsonObject.getBoolean("available")==true) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        ward_array = jsonArray;
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String ward_name = jsonObject1.getString("name");
                            String ward_id = jsonObject1.getString("id");
                            Log.e("", "onResponse: " + ward_name);
//                            Log.i("", "onResponse: "+county );
                            // Toast.makeText(pre_landing.this, ""+subcounty, Toast.LENGTH_SHORT).show();
                            wards.add(ward_name);
//                            SubCountyName.add("Select County");
                        }
                    }

                    spinner3.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, wards));

                }catch (JSONException e){e.printStackTrace();}

            }

        }, new Response.ErrorListener() {

            @Override

            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }

        });

        int socketTimeout = 30000;

        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);

        requestQueue.add(stringRequest);

    }
    private void loadprofessions() {

        StringRequest stringRequest=new StringRequest(Request.Method.POST , APIs.BaseUrl+"user/specialisation", new Response.Listener<String>() {

            @Override

            public void onResponse(String response) {
                wards.clear();
                try{

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("status");

                    Log.e("", "onResponse: " + error);
                    if (error==true) {
                        JSONArray message=jObj.getJSONArray("data");
                        for(int i=0;i<message.length();i++) {
                            JSONObject myuser = message.getJSONObject(i);
                            String nane = myuser.getString("name");
                            specialization.add(nane);
                        }
                    }
                    specialization_spinner.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, specialization));

                }catch (JSONException e){e.printStackTrace();}

            }

        }, new Response.ErrorListener() {

            @Override

            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }

        });

        int socketTimeout = 30000;

        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);

        requestQueue.add(stringRequest);

    }
    private void savedata(){
      final   String reg_no = regNo.getText().toString();

        HashMap<String, String> user = session.getUserDetails();
        //final String user_id = user.get(session_manager.KEY_ID);

        //Toast.makeText(getApplicationContext(),"is=."+user_id,Toast.LENGTH_LONG).show();
        Log.e("", "savedata: start");
        // Tag used to cancel the request
        String tag_string_req = "req_save";
        pDialog.setTitle("saving data..please wait");
        pDialog.show();
     //   Log.e("", "url: "+APIs.BaseUrl+"user/update/"+user_id);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                APIs.BaseUrl+"user/update", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("", "save Response: " + response);
                pDialog.show();
                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean success = jObj.getBoolean("status");
                    Log.e("", "onResponse: " + success);
                    if (success==true) {

                        Toast.makeText(getApplicationContext(),"Saved successfully.",Toast.LENGTH_LONG).show();
                        //if(specialization_spinner.getSelectedItem().toString().equals("Farmer")){
                            startActivity(new Intent(getApplicationContext(), dashboard.class));
                       // }
//                        else {
//                            startActivity(new Intent(getApplicationContext(), ServiceprovidersActivity.class));
//                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Error occurred.",Toast.LENGTH_LONG).show();
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                Log.e("", "onErrorResponse: "+error.toString() );
                pDialog.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                int id= 1;
                Map<String, String> params = new HashMap<String, String>();
                params.put("spec", "prof_name");
                prof_name="Vet";
                if(!prof_name.equals("Farmer")){
                    params.put("reg_no", reg_no);

                }
                params.put("county_code", county_id);
                params.put("sub_county", subcounty_name);
                params.put("ward", ward_name);
                //params.put("id", user_id);
                Log.e("", "getParams: "+params );
                return params;

            }

        };
        int socketTimeout = 30000;

        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }



}
