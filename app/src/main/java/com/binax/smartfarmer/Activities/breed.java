package com.binax.smartfarmer.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.controller.AnimalAdapter;
import com.binax.smartfarmer.controller.breedAdapter;
import com.binax.smartfarmer.helpers.AppController;
import com.binax.smartfarmer.helpers.session_manager;
import com.binax.smartfarmer.model.AnimalModel;
import com.binax.smartfarmer.model.breedModel;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class breed extends AppCompatActivity {
    ArrayList<breedModel> bullList = new ArrayList<>();
    RecyclerView recyclerView;
    breedAdapter adapter;
    session_manager session;
    String access_token;
    private static final String TAG = "breed";
    ProgressDialog proDialog;
    int MY_SOCKET_TIMEOUT_MS = 200000;
    int MY_DEFAULT_MAX_RETRIES = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breed);
        recyclerView = findViewById(R.id.breedRecycler);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(false);
        proDialog= new ProgressDialog(this);

        session= new session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
        access_token = user.get(session_manager.KEY_ACCESS_TOKEN);
        Intent myintent = getIntent();
        String sire =myintent.getStringExtra("sire");
        Map<String, Object> myRequestMap = new HashMap<>();
        myRequestMap.put("sire", sire);
        try {
            loadBulls(myRequestMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private  void loadBulls(Map<String, Object> requestMap) throws Exception {
        // make network request
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Finding a suitable breed ... please wait.");
        pDialog.setCancelable(false);
        pDialog.show();
        String payload = new Gson().toJson(requestMap);
        Log.e(TAG, " Payload: " + payload);
        JsonObjectRequest request = new JsonObjectRequest( APIs.BaseUrl+"v1/api/breedAnimal",
                new JSONObject(payload),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pDialog.dismiss();
                        Log.e(TAG, "onResponse: response: " + response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            JSONArray bulls = jsonObject.getJSONArray("result");
                            for (int i = 0; i < bulls.length(); i++) {
                                //getting product object from json array
                                JSONObject animals = bulls.getJSONObject(i);
                                //adding the animal to  view
                                bullList.add(new breedModel(
                                        animals.getInt("id"),
                                        animals.getString("name"),
                                        animals.getString("owner_id"),
                                        animals.getString("dam"),
                                        animals.getString("sire"),
                                        animals.getString("milk_capacity"),
                                        animals.getString("price"),
                                        animals.getString("ai_code"),
                                        animals.getString("picture")
                                ));
                            }
                            // creating adapter object and setting it to recyclerview
                            adapter=new breedAdapter(breed.this,bullList);
                            recyclerView.setAdapter(adapter);
                            recyclerView.scheduleLayoutAnimation();
                            adapter.notifyDataSetChanged();
                            pDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String body;
                if (error.networkResponse.data != null) {
                    try {
                        body = new String(error.networkResponse.data, "UTF-8");
                        Log.e(TAG, "onErrorResponse: Error: " + body);
                        JSONObject jsonObject = new JSONObject(body);
                        String message = jsonObject.getString("message");
                        //Toast.makeText(RegisterAnimals.this, ""+message, Toast.LENGTH_SHORT).show();
                        SweetAlertDialog dialog = new SweetAlertDialog(breed.this, SweetAlertDialog.ERROR_TYPE);
                        dialog.setTitleText("An error occured,please make sure the registration number is unique");
                        dialog.setCancelable(false);
                        dialog.show();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(breed.this, "Sorry ", Toast.LENGTH_SHORT).show();
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+access_token);
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                MY_DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().add(request);
    }


}
