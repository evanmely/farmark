package com.binax.smartfarmer.Activities;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.*;
import android.os.Bundle;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.Activities.util.funcs;
import com.binax.smartfarmer.helpers.AppController;
import com.binax.smartfarmer.helpers.CheckNetworkStatus;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.helpers.GPSTracker;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class Register extends Activity {
EditText fname,lname,email,password,phone,cnpassword;
Button btnreg;
    Context mContext;
    RadioGroup group;
    String specialization_id = "0";
    int MY_SOCKET_TIMEOUT_MS = 200000;
    int MY_DEFAULT_MAX_RETRIES = 0;
RadioButton radioButton;
    double latitude; // latitude
    double longitude;
    GPSTracker gps;
    private static final String TAG = "Register";
    ProgressDialog pDialog;
    TextView tvsignin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_registration);
        fname= findViewById(R.id.fname);
        lname= findViewById(R.id.lname);
        email= findViewById(R.id.email_address);
        phone= findViewById(R.id.phone);
        password= findViewById(R.id.password);
        cnpassword= findViewById(R.id.cnpassword);
        btnreg = findViewById(R.id.btnsign_up);
        pDialog= new ProgressDialog(this);
        pDialog.setCancelable(false);
        tvsignin= findViewById(R.id.sign_in);
        group= findViewById(R.id.rg);
        mContext=this;
        tvsignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),login.class));
            }
        });
        RadioGroup genderRadioGroup =  findViewById(R.id.rg);
        genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            //set a listener on the RadioGroup
            @Override
            public void onCheckedChanged(RadioGroup view, @IdRes int checkedId) {
                //checkedId refers to the selected RadioButton
                if (checkedId == R.id.farmer) {
                    specialization_id = "0";
                } else {
                    specialization_id = "1";
                }
            }
        });
        if(Build.VERSION.SDK_INT>= 23) {
            mContext = this;

            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(Register.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            } else {
                Toast.makeText(mContext, "You need have granted permission", Toast.LENGTH_SHORT).show();
                gps = new GPSTracker(mContext, Register.this);

                // Check if GPS enabled
                if (gps.canGetLocation()) {

                     latitude = gps.getLatitude();
                     longitude = gps.getLongitude();

                    // \n is for new line
                  // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                } else {
                    // Can't get location.
                    // GPS or network is not enabled.
                    // Ask user to enable GPS/network in settings.
                    gps.showSettingsAlert();
                }
            }

        }



        //action list
        btnreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {
                    String first_name = fname.getText().toString();
                    String last_name = lname.getText().toString();
                    String my_email = email.getText().toString();
                    String pass = password.getText().toString();
                    String phone_no = phone.getText().toString();
                    String cnfpass = cnpassword.getText().toString();
                    if (!first_name.isEmpty() && !last_name.isEmpty() && !my_email.isEmpty() && !phone_no.isEmpty() && !pass.isEmpty() && !cnfpass.isEmpty()) {
                        //call register method

                        if (!email.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
                            email.setError("Invalid Email Address");
                        }
                        if (funcs.formatPhone(phone.getText().toString().trim()).equals("0")) {
                            Toast.makeText(Register.this, "Sorry, Invalid phone number", Toast.LENGTH_LONG).show();
//
                           }
                            else
                            {
                                Map<String, Object> myRequestMap = new HashMap<>();
                                myRequestMap.put("username", phone_no);
                                myRequestMap.put("first_name", first_name);
                                myRequestMap.put("last_name", last_name);
                                myRequestMap.put("middle_name", "");
                                myRequestMap.put("email", my_email);
                                myRequestMap.put("phone", phone_no);
                                myRequestMap.put("password", pass);
                                myRequestMap.put("specialization_id",specialization_id);
                                myRequestMap.put("ward_id", "");
                                myRequestMap.put("latitude", latitude);
                                myRequestMap.put("longitude", longitude);
                                myRequestMap.put("role","ROLE_USER");

                                try {
                                    register(myRequestMap);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                           // register(first_name, last_name, my_email, pass, phone_no, );
                            }
                            }

                    else{
                        //SweetAlertDialog mdialog =new SweetAlertDialog(Register.this, SweetAlertDialog.ERROR_TYPE);
                        Toast.makeText(Register.this, "Empty fields are not allowed", Toast.LENGTH_LONG).show();
                       // mdialog.show();
                    }

                }
                else{
                        SweetAlertDialog mdialog =new SweetAlertDialog(Register.this, SweetAlertDialog.ERROR_TYPE);
                        mdialog.setTitleText("Failed to connect to Internet,Please make  sure You have a working Internet  Plan");
                        mdialog.show();
                    }

            }
        });

        validatemail();

    }
    public boolean validatemail(){
        String emailInput = email.getText().toString().trim();
        if(emailInput.isEmpty()){
            email.setError("Email cannot be Empty");
            return false;
        }
        else if(!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()){
            email.setError("Email invalid");
            return false;
        }
        else {
            email.setError(null);
            return true;
        }
    }
    private  void register(Map<String, Object> requestMap) throws Exception{
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Registering ... please wait.");
        pDialog.setCancelable(false);
        pDialog.show();
        // make network r,equest
        String payload = new Gson().toJson(requestMap);
        Log.e(TAG, " Payload: " + payload);
        JsonObjectRequest request = new JsonObjectRequest( APIs.BaseUrl+"api/auth/signup",
                new JSONObject(payload),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pDialog.dismiss();
                        Log.e(TAG, "onResponse: response: " + response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            boolean success =jsonObject.getBoolean("success");
                            String msg=jsonObject.getString("message");
                            if(success==true){
                                SweetAlertDialog dialog = new SweetAlertDialog(Register.this, SweetAlertDialog.SUCCESS_TYPE);
                                dialog.setTitleText("Registration successful!");
                                dialog.setCancelable(false);
                                dialog.show();
                            }
                            else {
                                SweetAlertDialog dialog = new SweetAlertDialog(Register.this, SweetAlertDialog.ERROR_TYPE);
                                dialog.setTitleText(msg);
                                dialog.setCancelable(false);
                                dialog.show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String body;
                if (error.networkResponse.data != null) {
                    try {
                        body = new String(error.networkResponse.data, "UTF-8");
                        Log.e(TAG, "onErrorResponse: Error: " + body);
                        JSONObject jsonObject = new JSONObject(body);
                        String message = jsonObject.getString("message");
                        //Toast.makeText(Register.this, ""+message, Toast.LENGTH_SHORT).show();
                        SweetAlertDialog dialog = new SweetAlertDialog(Register.this, SweetAlertDialog.ERROR_TYPE);
                        dialog.setTitleText(message);
                        dialog.setCancelable(false);
                        dialog.show();

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(Register.this, "Sorry ", Toast.LENGTH_SHORT).show();
                }
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                MY_DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().add(request);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the

                    // contacts-related task you need to do.

                    gps = new GPSTracker(mContext, Register.this);

                    // Check if GPS enabled
                    if (gps.canGetLocation()) {

                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();

                        // \n is for new line
                        Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    } else {
                        // Can't get location.
                        // GPS or network is not enabled.
                        // Ask user to enable GPS/network in settings.
                        gps.showSettingsAlert();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    Toast.makeText(mContext, "You need to grant permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }
}
