package com.binax.smartfarmer.Activities;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.binax.smartfarmer.R;
import com.binax.smartfarmer.controller.DashboardAdapter;
import com.binax.smartfarmer.helpers.session_manager;
import com.binax.smartfarmer.model.dashboardModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

public class newDashboard extends AppCompatActivity {
    private Context mContext=newDashboard.this;
    private RecyclerView recyclerView;
    private DashboardAdapter adapter;
    private List<dashboardModel> dashboardList;

    RelativeLayout rlTop;
    AppBarLayout Appbar;
    CollapsingToolbarLayout CoolToolbar;
    Toolbar toolbar;
    boolean ExpandedActionBar = true;
    CardView myFarm, myServices, profile,market;
    NavigationView navigation;
    DrawerLayout mdrawerLayout;
    session_manager session;
    ActionBarDrawerToggle drawerToggle;
    private View  mview;
    Dialog dl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_dashboard);
        rlTop=findViewById(R.id.rltop);
        Appbar = findViewById(R.id.appbar);
        CoolToolbar = findViewById(R.id.ctolbar);
        toolbar =  findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        CoolToolbar.setTitle("");
        initInstances();

        Appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (Math.abs(verticalOffset) > 200){
                    ExpandedActionBar = false;
                    CoolToolbar.setTitle("My Dashboard");
                    rlTop.setVisibility(View.GONE);
                    invalidateOptionsMenu();
                } else {
                    ExpandedActionBar = true;
                    CoolToolbar.setTitle("");
                    rlTop.setVisibility(View.VISIBLE);
                    invalidateOptionsMenu();
                }
            }
        });

        ImageView imgProfile=findViewById(R.id.profile_image);
        Glide.with(mContext)
                .load(R.drawable.user)
                .apply(RequestOptions.circleCropTransform())
                .into(imgProfile);

        recyclerView = findViewById(R.id.recycler_view);
        dashboardList = new ArrayList<>();
        adapter = new DashboardAdapter(mContext,dashboardList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        getMenu();
    }

    private void getMenu() {

        dashboardList.add(new dashboardModel(R.drawable.myfarm,"Myfarm"));
        dashboardList.add(new dashboardModel(R.drawable.market,"Market"));
        dashboardList.add(new dashboardModel(R.drawable.value,"Advertisement"));
        dashboardList.add(new dashboardModel(R.drawable.services,"Services"));


        adapter.notifyDataSetChanged();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    public int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    private void initInstances() {
session=new session_manager(this);
        mdrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerToggle = new ActionBarDrawerToggle(this, mdrawerLayout, R.string.open, R.string.close);
        mdrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        (getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        navigation = (NavigationView) findViewById(R.id.navigation_view);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.profile:
                        startActivity(new Intent(getApplicationContext(),wallet.class));
                        // add navigation drawer item onclick method here
                        break;
                    case R.id.notifications:
                        addNotification();

                        break;
                    case R.id.services:

                        openDialog();
                        // startActivity(new Intent(getApplicationContext(), Bookings.class));
                        break;
                    case R.id.registerAnimals:
                        startActivity(new Intent(getApplicationContext(), RegisterAnimals.class));
                        break;
                    case R.id.help:
                        // startActivity(new Intent(getApplicationContext(), About.class));
                        break;

                    case R.id.settings:
                        menuItem.setVisible(false);
                        //Do some thing here
                        // add navigation drawer item onclick method here
                        break;
                    case R.id.logout:
                        session.logoutUser();
                        break;
                }
                return true;
            }
        });

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    public void openDialog() {
        final Dialog dialog = new Dialog(this); // Context, this, etc.
        dialog.setContentView(R.layout.alert_dialog1);;
        dialog.show();
    }
    //build notification
    public void addNotification() {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notification)
                        .setContentTitle("Evans is interested in your services")
                        .setContentText("please accept.")
                        .setDefaults(Notification.DEFAULT_ALL) // must requires VIBRATE permission
                        .setPriority(NotificationCompat.PRIORITY_HIGH); //must give priority to High, Max which will considered as heads-up notification

    }
}
