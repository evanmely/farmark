package com.binax.smartfarmer.Activities;


import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.Activities.util.funcs;
import com.binax.smartfarmer.helpers.AppController;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.helpers.CheckNetworkStatus;
import com.binax.smartfarmer.helpers.session_manager;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class login extends AppCompatActivity {
    //private static final String TAG = Register.class.getSimpleName();
    //session_manager mysession;
    Button btnlogin;
    int MY_SOCKET_TIMEOUT_MS = 200000;
    int MY_DEFAULT_MAX_RETRIES = 0;
    session_manager session;
   // SweetAlertDialog dialog;
    TextView SignUp;
    private static final String TAG = "login";
    EditText phonetxt, passwordtxt;
    private ProgressDialog pDialog;
    String Login_Url;
    //private SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_new);
        Login_Url = APIs.BaseUrl+"api/auth/signin";
        btnlogin = findViewById(R.id.login);
        SignUp = findViewById(R.id.sign_up);
        session=new session_manager(this);
        passwordtxt = findViewById(R.id.password);
        passwordtxt.setText("password");
        phonetxt = findViewById(R.id.phoneNumber);
        phonetxt.setText("0721536635");
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        AppController apc = new AppController();
        pDialog=new ProgressDialog(this);
        OkHttpClient client = new OkHttpClient();
        client.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }

        });
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, Object> myRequestMap = new HashMap<>();
                myRequestMap.put("username", phonetxt.getText().toString());
                myRequestMap.put("password", passwordtxt.getText().toString());
                if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {
                    try {
                        mylogin(myRequestMap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else{
                    SweetAlertDialog dialog = new SweetAlertDialog(login.this, SweetAlertDialog.ERROR_TYPE);
                    dialog.setTitleText("Failed to connect to Internet,Please make  sure You have a working Internet  Plan");
                    dialog.setCancelable(false);
                    dialog.show();
                }
            }
        });
        SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Register.class));
            }
        });
    }


    private  void mylogin(Map<String, Object> requestMap) throws Exception {
        // make network request
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Logging in ...");
        pDialog.setCancelable(true);
        pDialog.show();
        String payload = new Gson().toJson(requestMap);
        Log.e(TAG, "fetchbranches Payload: " + payload);
        JsonObjectRequest request = new JsonObjectRequest( APIs.BaseUrl+"api/auth/signin",
                new JSONObject(payload),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pDialog.dismiss();
                        Log.e(TAG, "onResponse: response: " + response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            String access_token  =jsonObject.getString("accessToken");

                            if(jsonObject.has("accessToken")){
                                session.createLoginSession(access_token);
                                checkUser(access_token);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ServiceCall", error.toString());
                pDialog.dismiss();
                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){
                    Log.e(TAG, "pass : ");
                    switch(response.statusCode){
                        case 500:
                            String  json = new String(response.data);
                            try {
                                JSONObject jsonObject = new JSONObject(json);
                                String message = jsonObject.getString("message");
                                //Toast.makeText(login.this, ""+message, Toast.LENGTH_SHORT).show();
                                SweetAlertDialog dialog = new SweetAlertDialog(login.this, SweetAlertDialog.ERROR_TYPE);
                                dialog.setTitleText(message);
                                dialog.setCancelable(false);
                                dialog.show();

                            } catch (JSONException j) {
                                Log.e(TAG, "onErrorResponse: "+ j);
                                j.printStackTrace();
                            }
                            break;
                    }
                }
            }

        });
        request.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                MY_DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().add(request);
    }


    public void checkUser(final String access_token){
        StringRequest stringRequest=new StringRequest(Request.Method.GET, APIs.BaseUrl+"v1/api/user-details", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    JSONObject result=jsonObject.getJSONObject("result");
                    int sp=result.getInt("specialization_id");
                    if(sp==1){
                        Log.e("ServiceCall","pass h");
                        startActivity(new Intent(getApplicationContext(),SPctivity.class));

                    }
                    else{
                        startActivity(new Intent(getApplicationContext(),newDashboard.class));
                    }

                }catch (JSONException e){e.printStackTrace();}

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }

        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+access_token);
                return headers;
            }
        };
        Volley.newRequestQueue(this).add(stringRequest);
    }



    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
