package com.binax.smartfarmer.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.controller.ProfessionalsAdapter;
import com.binax.smartfarmer.controller.ServiceProviderAdapter;
import com.binax.smartfarmer.helpers.session_manager;
import com.binax.smartfarmer.model.ProfessionalsModel;
import com.binax.smartfarmer.model.ServiceProviders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ProfList extends AppCompatActivity {
    ArrayList<ServiceProviders>professionals;
    ServiceProviderAdapter adapter;
    RecyclerView myrecycler;
    String access_token;
    session_manager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prof_list);
        myrecycler= findViewById(R.id.proff_recycler);
        session= new session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
        access_token = user.get(session_manager.KEY_ACCESS_TOKEN);
        Professionals ();
    }
    private void Professionals () {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("loading service providers ... please wait.");
        pDialog.setCancelable(false);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, APIs.BaseUrl+"v1/api/serviceProviders",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        Log.e("", "onResponse: "+response );
                        professionals=new ArrayList<>();
                        try {
                            //converting the string to json array object
                            JSONObject obj = new JSONObject(response);
                            JSONArray data = obj.getJSONArray("result");
                            for (int i = 0; i < data.length(); i++) {
                                //getting product object from json array
                                JSONObject professional = data.getJSONObject(i);
                                //adding the animal to  view
                                professionals.add(new ServiceProviders(
                                        professional.getInt("id"),
                                        professional.getString("first_name"),
                                        professional.getString("last_name"),
                                        professional.getString("phone"),
                                        professional.getString("specialization_id"),
                                        professional.getDouble("latitude"),
                                        professional.getDouble("longitude")

                                ));
                            }
                            // creating adapter object and setting it to recyclerview
                            adapter=new ServiceProviderAdapter(ProfList.this,professionals);
                            myrecycler.setAdapter(adapter);
                            myrecycler.scheduleLayoutAnimation();
                            adapter.notifyDataSetChanged();
                            pDialog.dismiss();
                            // adapter.setOnItemClickListener(this);
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), " There was an ERROR" + e, Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(" error",error.toString());
                        Toast.makeText(getApplicationContext(), " An ERROR occurred" , Toast.LENGTH_LONG).show();
                        pDialog.dismiss();
                    }
                })
        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+access_token);
                return headers;
            }

        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
}
