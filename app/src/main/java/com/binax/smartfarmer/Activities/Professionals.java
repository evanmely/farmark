package com.binax.smartfarmer.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.controller.AnimalAdapter;
import com.binax.smartfarmer.controller.MarketAdapter;
import com.binax.smartfarmer.controller.ProfessionalsAdapter;
import com.binax.smartfarmer.helpers.session_manager;
import com.binax.smartfarmer.model.MarketModel;
import com.binax.smartfarmer.model.ProfessionalsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Professionals extends AppCompatActivity {
    session_manager session;
    ArrayList<ProfessionalsModel>professionals;
    ProgressDialog proDialog;
    ProfessionalsAdapter adapter;
    RecyclerView myrecycler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_professionals);
        proDialog= new ProgressDialog(this);
        myrecycler= findViewById(R.id.professional_recycler);
        Professionals ();

    }
    private void Professionals () {
        session= new session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
       // final String user_id = user.get(session_manager.KEY_ID);
        professionals=new ArrayList<>();
        proDialog.setTitle("Loading professionals. Please wait...");
        proDialog.setCancelable(false);
        proDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, APIs.BaseUrl+"service/get/testapi",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        proDialog.dismiss();
                        Log.e("", "onResponse: "+response );
                        try {
                            //converting the string to json array object
                            JSONObject obj = new JSONObject(response);
                            JSONArray data = obj.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                //getting product object from json array
                                JSONObject professional = data.getJSONObject(i);
                                //adding the animal to  view
                                professionals.add(new ProfessionalsModel(
                                        professional.getInt("id"),
                                        professional.getString("first_name"),
                                        professional.getString("first_name"),
                                        professional.getString("email"),
                                        professional.getString("phone"),
                                        professional.getString("ward_id")
                                ));
                            }
                            // creating adapter object and setting it to recyclerview
                            adapter=new ProfessionalsAdapter(Professionals.this,professionals);
                            myrecycler.setAdapter(adapter);
                            myrecycler.scheduleLayoutAnimation();
                            adapter.notifyDataSetChanged();
                            proDialog.dismiss();
                            // adapter.setOnItemClickListener(this);
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), " There was an ERROR" + e, Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(" error",error.toString());
                        Toast.makeText(getApplicationContext(), " An ERROR occurred" , Toast.LENGTH_LONG).show();
                        proDialog.dismiss();
                    }
                })
               {
                   @Override
                   protected Map<String, String> getParams() {
                       // Posting params to register url
                       Map<String, String> params = new HashMap<String, String>();
                       params.put("type_of_service", "AI provider");
                       return params;
                   }

                 };

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
}
