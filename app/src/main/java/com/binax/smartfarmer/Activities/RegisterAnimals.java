package com.binax.smartfarmer.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.helpers.AppController;
import com.binax.smartfarmer.helpers.session_manager;
import com.binax.smartfarmer.model.Datum;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.view.View;
import android.widget.*;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegisterAnimals extends AppCompatActivity {
TextInputEditText nametx,dam,sire,milk,uid,dob,RegNo,number;
Button btnregan;
ImageView pic;
Bitmap bitmap;
session_manager session;
Spinner myspinner,poultry_spinner,animal_category,breed_type;
ArrayList<String> categoryName;
    final int REQUEST_CODE_GALLERY = 111;
    int MY_SOCKET_TIMEOUT_MS = 200000;
    int MY_DEFAULT_MAX_RETRIES = 0;
    DatePickerDialog picker;
    String catName=null;
    ProgressDialog dialog;
    String id=null;
    static  int location_id;
    String category_id=null;
    String selectedBreed=null;
    String access_token;
    List<Datum> CategoryList;
    private static final String TAG = "RegisterAnimals";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_animals);
        nametx=findViewById(R.id.name);
        dam=findViewById(R.id.dam);
        RegNo =findViewById(R.id.RegNo);
        pic=findViewById(R.id.image);
        sire=findViewById(R.id.sire);
        milk=findViewById(R.id.milk_capacity);
        dob=findViewById(R.id.date_of_birth);
        dob.setInputType(InputType.TYPE_NULL);
        number=findViewById(R.id.number);
        btnregan=findViewById(R.id.btnregan);
        categoryName=new ArrayList<>();
        loadCategories();
        dialog= new ProgressDialog(this);
        animal_category =findViewById(R.id.category_type);
        breed_type =findViewById(R.id.breed_type);
       poultry_spinner =findViewById(R.id.poultry_breed);


        session= new session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
        access_token = user.get(session_manager.KEY_ACCESS_TOKEN);

//        ArrayAdapter<String> status=new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.categories));
//        status.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        myspinner.setAdapter(status);
        ArrayAdapter<String> poultry=new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.poultry_breed));
        poultry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        poultry_spinner.setAdapter(poultry);

        ArrayAdapter<String> breed=new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.cow_breed));
        breed.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        breed_type.setAdapter(breed);
        breed_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedBreed =  breed_type.getItemAtPosition(breed_type.getSelectedItemPosition()).toString();
                String catId=selectedBreed.split("-")[0];
                selectedBreed =catId;


                    Log.e(TAG, "onItemSelected: "+selectedBreed );



            }

            @Override

            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });
        animal_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedName =  animal_category.getItemAtPosition(animal_category.getSelectedItemPosition()).toString();
                String catId=selectedName.split("-")[0];

                loadBreeds(catId);

            }

            @Override

            public void onNothingSelected(AdapterView<?> adapterView) {


            }

        });

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(RegisterAnimals.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                dob.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //read external storage
                ActivityCompat.requestPermissions(
                        RegisterAnimals.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_GALLERY
                );
            }
        });
        btnregan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String myname= nametx.getText().toString();
                final  String mydam= dam.getText().toString();
                final  String mysire= sire.getText().toString();
                final  String mymilk= milk.getText().toString();
                final String mydob= dob.getText().toString();
                //final String mytype= myspinner.getSelectedItem().toString();
                final String regno= RegNo.getText().toString();
                Map<String, Object> myRequestMap = new HashMap<>();
                myRequestMap.put("breed_id", selectedBreed);
                myRequestMap.put("animal_registration_number", regno);
                myRequestMap.put("gender_id", 1);
                myRequestMap.put("name", myname);
                myRequestMap.put("sire", mysire);
                myRequestMap.put("date_born", mydob);
                myRequestMap.put("dam", mydam);
                myRequestMap.put("milk_capacity", mymilk);
                myRequestMap.put("picture", "");
                myRequestMap.put("description", "");
                myRequestMap.put("disorders", "");

                try {
                    register(myRequestMap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }
    private  void register(Map<String, Object> requestMap) throws Exception {
        // make network request
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Registering Animals ... please wait.");
        pDialog.setCancelable(false);
        pDialog.show();
        String payload = new Gson().toJson(requestMap);
        Log.e(TAG, " Payload: " + payload);
        JsonObjectRequest request = new JsonObjectRequest( APIs.BaseUrl+"v1/api/animals/register",
                new JSONObject(payload),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pDialog.dismiss();
                        Log.e(TAG, "onResponse: response: " + response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            boolean success =jsonObject.getBoolean("success");
                            String msg=jsonObject.getString("message");
                            if(success==true) {
                                SweetAlertDialog dialog = new SweetAlertDialog(RegisterAnimals.this, SweetAlertDialog.SUCCESS_TYPE);
                                dialog.setTitleText("Animal Registration successful!");
                                dialog.setCancelable(false);
                                dialog.show();
                            }
                            else {
                                SweetAlertDialog dialog = new SweetAlertDialog(RegisterAnimals.this, SweetAlertDialog.ERROR_TYPE);
                                dialog.setTitleText(msg);
                                dialog.setCancelable(false);
                                dialog.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String body;
                if (error.networkResponse.data != null) {
                    try {
                        body = new String(error.networkResponse.data, "UTF-8");
                        Log.e(TAG, "onErrorResponse: Error: " + body);
                        JSONObject jsonObject = new JSONObject(body);
                        String message = jsonObject.getString("message");
                        //Toast.makeText(RegisterAnimals.this, ""+message, Toast.LENGTH_SHORT).show();
                        SweetAlertDialog dialog = new SweetAlertDialog(RegisterAnimals.this, SweetAlertDialog.ERROR_TYPE);
                        dialog.setTitleText("An error occured,please make sure the registration number is unique");
                        dialog.setCancelable(false);
                        dialog.show();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(RegisterAnimals.this, "Sorry ", Toast.LENGTH_SHORT).show();
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+access_token);
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                MY_DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().add(request);
    }

    private void registerAnimals(){
        // Tag used to cancel the request
        String tag_string_req = "req_register";
        dialog.setTitle("Registering  Your Animals Please wait...");
        dialog.setCancelable(true);
        dialog.show();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                APIs.BaseUrl+"animals/register", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("", "Register Response: " + response);
                dialog.dismiss();
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("available");
                    Log.e("", "onResponse: " + error);
                    Log.e("", "onResponse: "+response);
                    if (error==true) {
                        Toast.makeText(RegisterAnimals.this, "Animals  Registered Successfully", Toast.LENGTH_LONG).show();

                    }
                    else
                    {
                        Toast.makeText(RegisterAnimals.this, "Registration error,please try again", Toast.LENGTH_LONG).show();
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("", "Registration Error: " + error.getMessage());
//                SweetAlertDialog dialog =new SweetAlertDialog(RegisterAnimals.this, SweetAlertDialog.ERROR_TYPE);
//                dialog.setTitleText("An error occurred  while registering.Please Try Again");
//                dialog.show();
//                dialog.dismiss();
            }
        }) {


        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
    public void loadCategories(){
        StringRequest stringRequest=new StringRequest(Request.Method.GET, APIs.BaseUrl+"v1/api/animals/get_category", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject=new JSONObject(response);



                        JSONArray jsonArray=jsonObject.getJSONArray("result");
                        for(int i=0;i<jsonArray.length();i++){

                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            catName=jsonObject1.getString("category_name");
                            id=jsonObject1.getString("id");
                            categoryName.add(id+"-"+catName);

                        }


                    animal_category.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, categoryName));

                }catch (JSONException e){e.printStackTrace();}

            }

        }, new Response.ErrorListener() {

            @Override

            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }

        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+access_token);
                return headers;
            }

        };

        Volley.newRequestQueue(this).add(stringRequest);

    }

    public void loadBreeds(String cat_id){
        StringRequest stringRequest=new StringRequest(Request.Method.GET, APIs.BaseUrl+"v1/api/animals/get_breed/"+cat_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    CategoryList = new ArrayList<>();
                    ArrayList<String>br= new ArrayList<>();
                    JSONObject jsonObject=new JSONObject(response);
                    JSONObject json=jsonObject.getJSONObject("result");
                    JSONArray jsonArray=json.getJSONArray("breeds");
                    Datum dat=new Datum();
                    for(int i=0;i<jsonArray.length();i++){

                        JSONObject jsonObject1=jsonArray.getJSONObject(i);
                        catName=jsonObject1.getString("breed_name");
                       // br.add(catName);
                        id=jsonObject1.getString("id");
                        dat.setId(Integer.parseInt(id));
                        dat.setName(catName);
                        br.add(id+"-"+catName);
                        CategoryList.add(dat);

                    }
                    breed_type.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, br));

                }catch (JSONException e){e.printStackTrace();}

            }

        }, new Response.ErrorListener() {

            @Override

            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }

        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+access_token);
                return headers;
            }

        };

        Volley.newRequestQueue(this).add(stringRequest);

    }



    @Override
    public void onRequestPermissionsResult ( int requestCode, @NonNull String[] permissions,
                                             @NonNull int[] grantResults){
        if (requestCode == REQUEST_CODE_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, REQUEST_CODE_GALLERY);
            } else {
                Toast.makeText(this, "You DON'T HAVE PERMISSION", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    @Override
    protected void onActivityResult ( int requestCode, int resultCode, @Nullable Intent data){
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                bitmap = BitmapFactory.decodeStream(inputStream);
                pic.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }
    //convert to encodedString
    private String imagetoString (Bitmap bitmap) {
        String encodedImage = null;
        if (bitmap==null) {
            Bitmap myimage = ((BitmapDrawable) pic.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            myimage.compress(Bitmap.CompressFormat.PNG, 50, baos);
            byte[] data = baos.toByteArray();
            encodedImage = Base64.encodeToString(data, Base64.DEFAULT);

        } else {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            byte[] imageBytes = outputStream.toByteArray();
            encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);


        }
        return "data:image/png;base64," + encodedImage;
    }
}
