package com.binax.smartfarmer.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.controller.AnimalAdapter;
import com.binax.smartfarmer.controller.MarketAdapter;
import com.binax.smartfarmer.helpers.session_manager;
import com.binax.smartfarmer.model.MarketModel;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Market extends AppCompatActivity {
session_manager session;
ArrayList<MarketModel>marketList=new ArrayList<>();
ProgressDialog proDialog;
MarketAdapter adapter;
RecyclerView myrecycler;
public String access_token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market);
        proDialog= new ProgressDialog(this);
        myrecycler= findViewById(R.id.recycler);
        session=new session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
        access_token = user.get(session_manager.KEY_ACCESS_TOKEN);
        market ();

    }
    private void market () {

        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading market. Please wait...");
        pDialog.setCancelable(true);
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, APIs.BaseUrl+"v1/api/market/get",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        Log.e("", "onResponse: "+response );
                        try {
                            //converting the string to json array object
                            JSONObject obj = new JSONObject(response);
                            JSONArray data = obj.getJSONArray("result").getJSONObject(0).getJSONArray("marketItems");
                            for (int i = 0; i < data.length(); i++) {
                                //getting product object from json array
                                JSONObject animal = data.getJSONObject(i);
                                //adding the animal to  view
                                JSONObject breeds=animal.getJSONObject("breed");

                                marketList.add(new MarketModel(
                                        animal.getInt("id"),
                                        animal.getString("name"),
                                        animal.getString("createdBy"),
                                        breeds.getString("breed_name"),
                                        animal.getString("price"),
                                        animal.getString("picture"),
                                        animal.getString("milk_capacity")

                                ));
                            }
                            // creating adapter object and setting it to recyclerview
                            adapter=new MarketAdapter(Market.this,marketList);
                            myrecycler.setAdapter(adapter);
                            myrecycler.scheduleLayoutAnimation();
                            adapter.notifyDataSetChanged();
                            pDialog.dismiss();
                            // adapter.setOnItemClickListener(this);
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), " There was an ERROR" + e, Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(" error",error.toString());
                        Toast.makeText(getApplicationContext(), " An ERROR occurred" , Toast.LENGTH_LONG).show();
                        pDialog.dismiss();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+access_token);
                return headers;
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
}
