package com.binax.smartfarmer.Activities.util;

public class funcs {

    public static String formatPhone(String msisdn) {
        String start_char = String.valueOf(msisdn.charAt(0));
        String phoneNumber;

        int msisdn_length = msisdn.length();
        if (start_char.equals("+") && msisdn_length == 13) {
            phoneNumber = msisdn.substring(1);
        } else if (start_char.equals("2") && msisdn_length == 12) {
            phoneNumber = msisdn;
        } else if (start_char.equals("0") && msisdn_length == 10) {
            msisdn = "+254" + msisdn;
            phoneNumber = msisdn.replace("+2540", "254");
        } else if (start_char.equals("7") && msisdn_length == 9) {
            phoneNumber = "254" + msisdn;
        } else {
            phoneNumber = "0";
        }

        return phoneNumber;
    }

}
