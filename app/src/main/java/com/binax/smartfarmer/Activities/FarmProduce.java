package com.binax.smartfarmer.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.controller.FarmProduceAdapter;
import com.binax.smartfarmer.helpers.AppController;
import com.binax.smartfarmer.model.FarmProduceModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FarmProduce extends AppCompatActivity {
        //List<FarmProduceModel> farm = new ArrayList<>();
    ArrayList<FarmProduceModel> farm = new ArrayList<>();
        private RecyclerView recyclerView;
        private FarmProduceAdapter pAdapter;
        ProgressDialog proDialog;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_farm_produce);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
            pAdapter = new FarmProduceAdapter(farm,this);
            // Create grids with 2 items in a row
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(pAdapter);
            proDialog= new ProgressDialog(this);

            pupulateProduce();
            pAdapter.notifyDataSetChanged();

        }

        private void pupulateProduce() {
            String tag_string_req = "farm_produce";
            proDialog.setTitle("Loading Products");
            proDialog.show();
            proDialog.setCancelable(false);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, APIs.BaseUrl+"service/products",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            proDialog.dismiss();
                            Log.e("", "onResponse: "+response );
                    try {
                        JSONObject jObj = new JSONObject(response);
                        Boolean available=jObj.getBoolean("available");
                        if(available==true){

                            JSONArray data = jObj.getJSONArray("data");
                            for(int i=0;i<data.length();i++){
                                JSONObject val= data.getJSONObject(i);
                                Log.e("", "onResponse: "+val.getString("name"));
                                farm.add(new FarmProduceModel(
                                        val.getInt("id"),
                                        val.getString("name"),
                                        val.getString("pic"),
                                        val.getString("price"),
                                        val.getString("quantity")

                                ));
                            }
                            // creating adapter object and setting it to recyclerview

                            recyclerView.setAdapter(pAdapter);
                            recyclerView.scheduleLayoutAnimation();
                            pAdapter.notifyDataSetChanged();
                            proDialog.dismiss();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Payment", "payment Error: " + error.getMessage());
                    proDialog.dismiss();


                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    // Posting params to register url
                    Map<String, String> params = new HashMap<String, String>();
                    return params;

                }

            };

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(stringRequest, tag_string_req);
        }

    }

