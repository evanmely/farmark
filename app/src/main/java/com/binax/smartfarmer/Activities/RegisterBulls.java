package com.binax.smartfarmer.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.binax.smartfarmer.APIs.APIs;
import com.binax.smartfarmer.R;
import com.binax.smartfarmer.helpers.AppController;
import com.binax.smartfarmer.helpers.session_manager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

//import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegisterBulls extends AppCompatActivity {
    TextInputEditText nametx,dam,AiCode,sire,milk,price,bullDescription,Calving_ease;
    Button btnregbull;
    ImageView bullPic;
    int MY_SOCKET_TIMEOUT_MS = 200000;
    int MY_DEFAULT_MAX_RETRIES = 0;
    final int REQUEST_CODE_GALLERY = 111;
    Bitmap bitmap;
    private static final String TAG = "RegisterBulls";
    session_manager session;
    ProgressDialog mydialog;
    Spinner myspinner;
    String access_token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_bulls);
        mydialog= new ProgressDialog(this);
session=new session_manager(this);
nametx=findViewById(R.id.name);
AiCode=findViewById(R.id.ai_code);
Calving_ease=findViewById(R.id.Calving_ease);
dam=findViewById(R.id.my_dam);
milk=findViewById(R.id.milk_capacity);
sire=findViewById(R.id.my_sire);
btnregbull=findViewById(R.id.btnregbull);
bullPic=findViewById(R.id.bullPic);
price=findViewById(R.id.price);
bullDescription=findViewById(R.id.bullDescription);



        session= new session_manager(this);
        HashMap<String, String> user = session.getUserDetails();
        access_token = user.get(session_manager.KEY_ACCESS_TOKEN);

        bullPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //read external storage
                ActivityCompat.requestPermissions(
                        RegisterBulls.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_GALLERY
                );
            }
        });
btnregbull.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        Map<String, Object> myRequestMap = new HashMap<>();
        myRequestMap.put("ai_code", AiCode.getText().toString());
        myRequestMap.put("name", nametx.getText().toString());
        myRequestMap.put("sire", sire.getText().toString());
        myRequestMap.put("dam", dam.getText().toString());
        myRequestMap.put("milk_capacity", milk.getText().toString());
        myRequestMap.put("picture", "");
        myRequestMap.put("description", bullDescription.getText().toString());
        myRequestMap.put("price", price.getText().toString());
        myRequestMap.put("calving_ease", Calving_ease.getText().toString());


        try {
            register(myRequestMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
});
    }
//    private void RegisterBulls(){
//        final String myname= nametx.getText().toString();
//        final  String mydam= dam.getText().toString();
//        final  String mysire= sire.getText().toString();
//        final  String mymilk= milk.getText().toString();
//        //final String mydob= dob.getText().toString();
//       // final String bred_type= myspinner.getSelectedItem().toString();
//        final String ai_code= AiCode.getText().toString();
//       // final String regno= RegNo.getText().toString();
//
//        session= new session_manager(this);
//        HashMap<String, String> user = session.getUserDetails();
//        //final String owner_id = user.get(session_manager.KEY_ID);
//        //uid.setText(owner_id);
//        Log.e("", "registerUser: start");
//        // Tag used to cancel the request
//        String tag_string_req = "req_register";
//        mydialog.setTitle("Registering  Please wait...");
//        mydialog.setCancelable(false);
//        mydialog.show();
//        StringRequest strReq = new StringRequest(Request.Method.POST,
//                APIs.BaseUrl+"animal/Bullregister", new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                Log.d("", "Register Response: " + response);
//                mydialog.dismiss();
//                try {
//
//                    JSONObject jObj = new JSONObject(response);
//                    boolean error = jObj.getBoolean("success");
//                    Log.e("", "onResponse: " + error);
//                    Log.e("", "onResponse: "+response);
//                    if (error==true) {
//                        Toast.makeText(getApplicationContext(),"Sucessfully registered",Toast.LENGTH_LONG).show();
//                    }
//                    else
//                    {
//                        Toast.makeText(getApplicationContext(),"Failed .try again",Toast.LENGTH_LONG).show();
//
//                    }
//                }
//                catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e("", "Registration Error: " + error.getMessage());
//
//                mydialog.dismiss();
//            }
//        }) {
//
//            @Override
//            protected Map<String, String> getParams() {
//                // Posting params to register url
//                int id=1;
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("name", myname);
//                params.put("dam", mydam);
//                //params.put("reg_no", regno);
//                params.put("milk_volume", mymilk);
//                params.put("sire", mysire);
//                params.put("owner_id", String.valueOf( id));
//                params.put("ai_code", ai_code);
//                params.put("breed_type", "Friesien");
//                params.put("pic", imagetoString(bitmap));
//                params.put("longevity", "longevity");
//                params.put("fertility", "fertility");
//                return params;
//            }
//        };
//        // Adding request to request queue
//        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
//    }
    @Override
    public void onRequestPermissionsResult ( int requestCode, @NonNull String[] permissions,
                                             @NonNull int[] grantResults){
        if (requestCode == REQUEST_CODE_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, REQUEST_CODE_GALLERY);
            } else {
                Toast.makeText(this, "You DON'T HAVE PERMISSION", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    @Override
    protected void onActivityResult ( int requestCode, int resultCode, @Nullable Intent data){
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                bitmap = BitmapFactory.decodeStream(inputStream);
                bullPic.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }
    //convert to encodedString
    private String imagetoString (Bitmap bitmap) {
        String encodedImage = null;
        if (bitmap==null) {
            Bitmap myimage = ((BitmapDrawable) bullPic.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            myimage.compress(Bitmap.CompressFormat.PNG, 50, baos);
            byte[] data = baos.toByteArray();
            encodedImage = Base64.encodeToString(data, Base64.DEFAULT);
        } else {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            byte[] imageBytes = outputStream.toByteArray();
            encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        }
        return "data:image/png;base64," + encodedImage;
    }
    private  void register(Map<String, Object> requestMap) throws Exception {
        // make network request
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Registering Bulls ... please wait.");
        pDialog.setCancelable(false);
        pDialog.show();
        String payload = new Gson().toJson(requestMap);
        Log.e(TAG, " Payload: " + payload);
        JsonObjectRequest request = new JsonObjectRequest( APIs.BaseUrl+"v1/api/bulls/register",
                new JSONObject(payload),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pDialog.dismiss();
                        Log.e(TAG, "onResponse: response: " + response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response));
                            boolean success =jsonObject.getBoolean("success");
                            String msg=jsonObject.getString("message");
                            if(success==true) {
                                SweetAlertDialog dialog = new SweetAlertDialog(RegisterBulls.this, SweetAlertDialog.SUCCESS_TYPE);
                                dialog.setTitleText("Bull Registration successful!");
                                dialog.setCancelable(false);
                                dialog.show();
                            }
                            else {
                                SweetAlertDialog dialog = new SweetAlertDialog(RegisterBulls.this, SweetAlertDialog.ERROR_TYPE);
                                dialog.setTitleText(msg);
                                dialog.setCancelable(false);
                                dialog.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String body;
                if (error.networkResponse.data != null) {
                    try {
                        body = new String(error.networkResponse.data, "UTF-8");
                        Log.e(TAG, "onErrorResponse: Error: " + body);
                        JSONObject jsonObject = new JSONObject(body);
                        String message = jsonObject.getString("message");
                        //Toast.makeText(RegisterAnimals.this, ""+message, Toast.LENGTH_SHORT).show();
                        SweetAlertDialog dialog = new SweetAlertDialog(RegisterBulls.this, SweetAlertDialog.ERROR_TYPE);
                        dialog.setTitleText("An error occured,please make sure the AI Code  is unique");
                        dialog.setCancelable(false);
                        dialog.show();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(RegisterBulls.this, "Sorry ", Toast.LENGTH_SHORT).show();
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+access_token);
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                MY_DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().add(request);
    }
}
